#pragma once
#include "Definitions.h"
#include "glDisplayable.h"
#include <vector>
#include <GL/GLU.h>
#include <memory>

class glPointCloud:
	public glDisplayable
{
private:
	QGLBuffer m_posBuffer, m_indexBuffer, m_colBuffer;
	//std::vector<GLfloat> data;
	std::vector<tuple3f> pos;
	std::vector<tuple3f> col;
	std::vector<int> ind;
	bool isUpToDate;
public:
	typedef std::shared_ptr<glPointCloud> Ptr;

	glPointCloud(const scanning::Cloud::Ptr & cloud, bool useColor);
	~glPointCloud(void);

	void updateData(const scanning::Cloud::Ptr & cloud, bool useColor = true);

	virtual void glPrepare();
	virtual bool isGlPrepared();
	virtual void glUpdate();
	virtual bool isGlUpToDate();
	virtual void glDraw();

};

