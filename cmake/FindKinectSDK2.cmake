#Read EnvironmentVariables
#Kinect with windows 2 SDK. Sets KinectSDK2_LIBRARIES, KinectSDK2_INCLUDES


SET(KinectSDK2_ROOT_DIR $ENV{KINECTSDK20_DIR})
message(${KinectSDK2_ROOT_DIR})

#64bit library
SET(KinectSDK2_LIBRARIES "${KinectSDK2_ROOT_DIR}lib/x64/Kinect20.lib")
SET(KinectSDK2_INCLUDE_DIR "${KinectSDK2_ROOT_DIR}inc")
SET(KinectSDK2_DLLS "${KinectSDK2_ROOT_DIR}Assemblies/Microsoft.Kinect.dll")
SET(KinectSDK2_INCLUDES "${KinectSDK2_INCLUDE_DIR}/kinect.h")

####################   Macro   #######################
MACRO(CHECK_FILES _FILES _DIR)
	SET(_MISSING_FILES)
	FOREACH(_FILE ${${_FILES}})
		IF(NOT EXISTS "${_FILE}")
			SET(KinectSDK2_FOUND NO)
			get_filename_component(_FILE ${_FILE} NAME)
			SET(_MISSING_FILES "${_MISSING_FILES}${_FILE}, ")
		ENDIF()
	ENDFOREACH()
	IF(_MISSING_FILES)
		MESSAGE(STATUS "In folder \"${${_DIR}}\" not found files: ${_MISSING_FILES}")
		SET(KinectSDK2_FOUND NO)
	ENDIF()
ENDMACRO(CHECK_FILES)

MACRO(CHECK_DIR _DIR)
	IF(NOT EXISTS "${${_DIR}}")
		MESSAGE(STATUS "Folder \"${${_DIR}}\" not found.")
		SET(KinectSDK2_FOUND NO)
	ENDIF()
ENDMACRO(CHECK_DIR)



################## Checking ########################
SET(KinectSDK2_FOUND YES)
CHECK_DIR(KinectSDK2_ROOT_DIR)
CHECK_DIR(KinectSDK2_INCLUDE_DIR)
#CHECK_FILES(KinectSDK2_LIBRARIES KinectSDK2_LIB_DIR)
CHECK_FILES(KinectSDK2_INCLUDES KinectSDK2_INCLUDE_DIR)

MESSAGE(STATUS "KinectSDK2_FOUND - ${KinectSDK2_FOUND}.")
MESSAGE("KinectSDK2_LIBRARIES - ${KinectSDK2_LIBRARIES}.")
#if not found, set all to empty string...
IF(NOT KinectSDK2_FOUND)
	MESSAGE("**Warning: setting all kinect2 directories to \"\" ")
	SET(KinectSDK2_ROOT_DIR "")
	SET(KinectSDK2_LIBRARIES "")
	SET(KinectSDK2_INCLUDE_DIR "")
	SET(KinectSDK2_DLLS "")
	SET(KinectSDK2_INCLUDES "")
ENDIF()