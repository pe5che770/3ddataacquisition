#pragma once
#include <string>
#include <Windows.h>
#include "kinectProvider.h"
#include "fileWriteTask.h"

class RangeSensorDescription;
class kinect2RawDataProvider;

class RangeSensorRecorder
{
	RangeSensorDescription * _description;
	std::string outputFolder;
	bool isFirstFrame;
	fileWriteTask_raw::mode m;

public:
	RangeSensorRecorder(std::string outputFolder, RangeSensorDescription * description, fileWriteTask_raw::mode m = fileWriteTask_raw::DEPTH_COLOR);
	~RangeSensorRecorder(void);

	void recordCurrentFrame(kinectProvider * prov, int frameNr);
	void recordCurrentFrame(kinect2RawDataProvider * prov, int frameNr);

	void saveDescription();//to be implemented in description, alongside with load from file.
};

