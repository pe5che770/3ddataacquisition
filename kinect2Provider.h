#pragma once
#include "Definitions.h"
#include <kinect.h>
#include <Windows.h>
#include <vector>

#include "kinectProvider.h"


class cudaTool;

class kinect2Provider:public kinectProvider
{
private:
	std::vector<UINT16> depthBuffer;
	std::vector<float> depthFloatBuffer, normalFloatBuffer;
	std::vector<ColorSpacePoint> colorCoordinateBuffer;
	std::vector<RGBQUAD> colorBuffer;
	scanning::Cloud::Ptr cloud;
	scanning::Cloud::Ptr filteredCloud;


	//sensor description
	kinectV2Description kin2_description;
	//the sensor.
	IKinectSensor * m_pKinectSensor;
	IDepthFrameReader * m_pDepthReader;
	IColorFrameReader * m_pColorReader;
	ICoordinateMapper * m_pMapper;

	void initKinect();
	void initDepthStreamProperties();
	void initColorStreamProperties();

	bool resolve(HRESULT & res);


	//what about all this? kill it if possible
	int width, height;
	int width_color, height_color;
	unsigned int num_px_depth, num_px_color;
	float center_i, center_j;
	float h_fov, v_fov;
	float tan_h_fov_2, tan_v_fov_2;

public:

	kinect2Provider(providerType whatToLoad= DEPTH_ONLY);
	virtual ~kinect2Provider(void);

	virtual bool reloadData();
	virtual void recomputeClouds();

	virtual scanning::Cloud::Ptr & getPointCloud();
	virtual scanning::Cloud::Ptr & getSmoothedCloud();

	RangeSensorDescription * getDescription();

	virtual void getRawBuffers(std::vector<UINT16> & depth, 
		std::vector<RGBQUAD> & alignedColor){
			depth = depthBuffer;
			alignedColor = colorBuffer;
	}

	//compute x,y camera coordinates
	/*inline float cast_i2x(int i, float z);
	inline float cast_j2y(int i, float z);*/
};

