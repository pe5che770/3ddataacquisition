#include "demo_kinectSDK2.h"
#include "kinect2Provider.h"
#include "kinect2EventDrivenProvider.h"
#include "kinect1Provider.h"

//#include <Windows.h>
#include "playBackProvider.h"
#include "RangeSensorRecorder.h"
#include "rawKinect2Data.h"
#include "kinect2RawDataProvider.h"
#include "OpenNIProvider.h"
#include <iostream>

demo_kinectSDK2::demo_kinectSDK2(demoType type, std::string folder):
	kinectCloud(new scanning::Cloud())
{
	kinect = NULL;
	kinectRaw = NULL;
	rawData = NULL;
	win = NULL;
	pass =0;
	totalTime = 0;
	demo = type;
	recordData = false;

	recorder = NULL;
	if (type == FILEREADER){
		input_folder = folder;
	}
	else{
		output_folder = folder;
	}
	paused = false;
}


demo_kinectSDK2::~demo_kinectSDK2(void)
{
	if(kinect!= NULL){
		delete kinect;
	}
	if(recorder!= NULL){
		delete recorder;
	}
	if (kinectRaw != NULL){
		delete kinectRaw;
	}
	if(rawData != NULL){
		delete rawData;
	}

}

void demo_kinectSDK2::startStopRecording(){
	recordData = !recordData;
	std::cout << "recording: " << recordData << " \n";

	if (demo == KIN2_RAWDATA || demo == KIN2_RAWDATA_IR){
		//nothing
	}
	else if(recordData){
		//start by storing the sensor description.
		recorder->saveDescription();
	}
	else{
		//nothing
	}

}

void demo_kinectSDK2::startStopReading(){

}


void demo_kinectSDK2::qInit(){
	QGLFormat glFormat;
	glFormat.setVersion( 4,1 );
	glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
	glFormat.setSampleBuffers( true );
	win = new mainWindow(glFormat);

	win->getButton3()->setText("Pause");
	connect(win->getButton3(), SIGNAL(released()), this, SLOT(pause()));

	win->show();
	if (demo == KIN1){
		kinect = new kinect1Provider(kinectProvider::DEPTH_AND_COLOR);
		win->getButton1()->setText("Record");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		recorder = new RangeSensorRecorder(output_folder, kinect->getDescription());
	}
	else if (demo == ASUS_XTION){
		kinect = new OpenNIProvider();
		win->getButton1()->setText("Record");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		recorder = new RangeSensorRecorder(output_folder, kinect->getDescription());
	}
	else if(demo == KIN2_POLLING){
		kinect = new kinect2Provider();
		win->getButton1()->setText("Record");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		recorder = new RangeSensorRecorder(output_folder ,kinect->getDescription());
	}
	else if (demo == KIN2_EVENTDRIVEN){
		kinect = new kinect2EventDrivenProvider(kinectProvider::DEPTH_AND_COLOR);
		win->getButton1()->setText("Record");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		recorder = new RangeSensorRecorder(output_folder, kinect->getDescription());
	}
	else if(demo == FILEREADER){
		//kinectV2Description desc;
		kinect = new playBackProvider(input_folder);
		win->getButton2()->setText("Play Back");
		connect(win->getButton2(), SIGNAL(released()), this, SLOT(startStopReading()));
	}
	
	else if (demo == KIN2_RAWDATA){
		kinectRaw = new kinect2RawDataProvider(kinect2RawDataProvider::COLOR_DEPTH);
		win->getButton1()->setText("Record RAW");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		rawData = new rawKinect2Data();
		recorder = new RangeSensorRecorder(output_folder, kinectRaw->getDescription());
	}
	else if (demo == KIN2_RAWDATA_IR){
		kinectRaw = new kinect2RawDataProvider(kinect2RawDataProvider::COLOR_IR);
		win->getButton1()->setText("Record RAW");
		connect(win->getButton1(), SIGNAL(released()), this, SLOT(startStopRecording()));
		rawData = new rawKinect2Data();
		recorder = new RangeSensorRecorder(output_folder, kinectRaw->getDescription(), fileWriteTask_raw::DEPTH_IR);
	}
	
}

void demo_kinectSDK2::qFree(){
	delete win;
}

void demo_kinectSDK2::refresh(){
	
	if(paused){
		Sleep(15);
		return;
	}
	bool gotData = kinect->reloadData();

			
	if (gotData){

		kinect->recomputeClouds();

		if (pass % 2 == 0 && !recordData || pass % 10 == 0){
			win->getGLWidget()->makeCurrent();
			win->getGLWidget()->getSceneManager()->updateCloud("cloud", kinect->getPointCloud(), true);
			win->getGLWidget()->update();/**/
		}
		if(pass %20 == 0){
			totalTime = GetTickCount() - start;
			std::cout << "fps" << 20 *1000.0 / totalTime << "\n";
			totalTime = 0;
			start = GetTickCount();
		}

		pass++;

		if(recordData){
			recorder->recordCurrentFrame(kinect, pass);
		}
	}


}


void demo_kinectSDK2::refreshRawData()
{
	pass++;
	//std::cout << "Acquiring raw data ...\n";
	bool gotData = kinectRaw->reloadData();
	if(!gotData){
		std::cout << "duh\n";
		Sleep(rand()%10);
		return;
	}

	
	//kinectRaw->getRawBuffers(*rawData);



	if(recordData){
		//std::cout << rawData->k2;
		recorder->recordCurrentFrame(kinectRaw, pass);
		if (pass % 10 == 0){
			//3fps is enough for preview
			kinectRaw->recomputeClouds();
			win->getGLWidget()->makeCurrent();
			win->getGLWidget()->getSceneManager()->updateCloud("cloud", kinectRaw->getPointCloud(), true);
			win->getGLWidget()->update();//
		}
	}
	else{
		if(pass % 4 == 0){
			//5fps is enough for preview
			kinectRaw->recomputeClouds();
			win->getGLWidget()->makeCurrent();
			win->getGLWidget()->getSceneManager()->updateCloud("cloud", kinectRaw->getPointCloud(), true);
			win->getGLWidget()->update();//
		}
	}
	//yield...
	Sleep(10);
	
}

void demo_kinectSDK2::pause(){
	paused = ! paused;
}

void demo_kinectSDK2::run(){
	//dummy arguments;
	int argc = 0;
	char arg[] = "bla";
	char * args = arg;


	//set up window
	QApplication app(argc, &args);

	
	//add actions...
	//this->addActionToWindow();
	qInit();

	timer = new QTimer(this);
	if (demo == KIN2_RAWDATA || demo == KIN2_RAWDATA_IR){
		timer->connect(timer,SIGNAL(timeout()),this,SLOT(refreshRawData()));
	}
	else{
		timer->connect(timer,SIGNAL(timeout()),this,SLOT(refresh()));
	}
	timer->setInterval(10);
	timer->start();
	app.exec();

	qFree();
	delete timer;
}
