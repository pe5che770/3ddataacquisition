#include "fileWriteTask.h"

#include <qimage.h>
#include <qfile.h>
#include <sstream>
#include "Sensors.h"
#include "rawKinect2Data.h"
#include <fstream>
#include <iostream>

fileWriteTask::fileWriteTask(std::string foldername, RangeSensorDescription * desc, int frm)
{
	_folderName = foldername; _width = desc->width(); _height = desc->height();
	setAutoDelete(true);
	frame = frm;
}


fileWriteTask::~fileWriteTask(void)
{
}

void fileWriteTask::run(){
	QImage img(_width, _height,QImage::Format_RGB32);
	QImage depthImg(_width, _height,QImage::Format_RGB32);

	int idx = 0; 
	QRgb val;

	//fill in image data
	for(int j = 0; j < _height; j++){
		QRgb *rowData = (QRgb*)img.scanLine(j);
		QRgb *rowData_depth = (QRgb*)depthImg.scanLine(j);
		for(int i = 0; i < _width; i++){
			val= qRgb((int)color[idx].rgbRed,
				(int)color[idx].rgbGreen,
				(int)color[idx].rgbBlue);

			rowData[i] = val; 
			val = depth[idx];//qRgb(0,(depth[idx]>>8)&255,depth[idx]&255);
			rowData_depth[i] = val;
			idx++;
		}
	}

	//write the images.
	std::stringstream ss;
	ss <<_folderName << "/img_"<< frame <<".png";
	img.save(ss.str().c_str(), "PNG");
			
	std::stringstream ss2;
	ss2<< _folderName << "/depth_" << frame << ".png";
	depthImg.save(ss2.str().c_str(), "PNG");
	
}


//RAW DATA records....

fileWriteTask_raw::fileWriteTask_raw(std::string folder, int frm, bool save_meta_data, mode m):
	dataMode(m)
{
	std::cout << folder << "\n";
	_folderName = folder;
	frame = frm;
	saveMetaData = save_meta_data;
	data = new rawKinect2Data();
}

fileWriteTask_raw::~fileWriteTask_raw(){
	delete data;
}



void fileWriteTask_raw::storeDepthAndColor()
{
	QImage img(data->color_w, data->color_h,QImage::Format_RGB32);
	QImage depthImg(data->depth_w, data->depth_h,QImage::Format_RGB32);

	//this will be the depth coordinates in tenth of pixels.
	QImage col2Depth(data->color_w, data->color_h,QImage::Format_RGB32);

	int idx_c = 0; int idx_d = 0; 
	int w_c = data->color_w; int h_c = data->color_h;
	int w_d = data->depth_w; int h_d = data->depth_h;
	QRgb val, val2;
	UINT16 tmp1, tmp2;

	auto & color = data->rawColor;
	auto & depth = data->rawDepth;
	auto & color2depth = data->color2Depth;
	//fill in image data
	int _height = max(h_c, h_d);
	int _width = max(w_c, w_d);

	for(int j = 0; j < _height; j++){
		QRgb *rowData = NULL,  *rowData_depth = NULL, *rowData_c2d = NULL;
		if(j < h_c){
			rowData = (QRgb*)img.scanLine(j);
			rowData_c2d = (QRgb*)col2Depth.scanLine(j);
		}
		
		if(j < h_d){
			rowData_depth = (QRgb*)depthImg.scanLine(j);
		}
		for(int i = 0; i < _width; i++){

			if(j < h_c && i < w_c){
				val= qRgb((int)color[idx_c].rgbRed,
					(int)color[idx_c].rgbGreen,
					(int)color[idx_c].rgbBlue);
				rowData[i] = val; 

				if(idx_c> 180){
					idx_c = idx_c;
				}
				val = (int) color2depth[idx_c].X * 10;
				val2 = (int) color2depth[idx_c].Y * 10;
				val = val << 16;
				val = val | val2;
				rowData_c2d[i] = val;
				idx_c++;
			}
			if(j < h_d && i < w_d){
				val = depth[idx_d];//qRgb(0,(depth[idx]>>8)&255,depth[idx]&255);
				rowData_depth[i] = val;
				idx_d++;
			}
		}
	}

	frame = data->pass;

	//write the images.
	std::stringstream ss;
	ss << _folderName << "/img_" << frame << ".png";
	img.save(ss.str().c_str(), "PNG",100);
			
	std::stringstream ss2;
	ss2 << _folderName << "/depth_" << frame << ".png";
	depthImg.save(ss2.str().c_str(), "PNG");

	std::stringstream ss3;
	ss3 << _folderName << "/c2d_" << frame << ".png";
	col2Depth.save(ss3.str().c_str(), "PNG", 100);
	//std::cout << "wrote";

	if(saveMetaData){
		std::ofstream file;
		file.open(std::string(_folderName + "/sensor.kin2"));
		file << "depth " << data->depth_w << " " << data->depth_h << "\n";
		file << "color " << data->color_w << " " << data->color_h << "\n";
		file << "focal " << data->f_x << " " << data->f_y << "\n";
		file << "principal " << data->principal_x << " " << data->principal_y << "\n";
		file << "distortion " << data->k2 << " " << data->k4 << " " << data->k6 << "\n";
		
		file.close();
	}

}


void fileWriteTask_raw::storeDepthAndIR()
{

	QImage irImg(data->depth_w, data->depth_h,QImage::Format_RGB32);
	QImage depthImg(data->depth_w, data->depth_h,QImage::Format_RGB32);


	int idx_d = 0; 
	int w_d = data->depth_w; int h_d = data->depth_h;
	QRgb val, val2;
	UINT16 tmp1, tmp2;

	auto & ir = data->rawIR;
	auto & depth = data->rawDepth;
	
	//fill in image data
	for(int j = 0; j < h_d; j++){
		QRgb *rowData_ir = NULL,  *rowData_depth = NULL;
		
		rowData_ir = (QRgb*)irImg.scanLine(j);
		rowData_depth = (QRgb*)depthImg.scanLine(j);
		
		for(int i = 0; i < w_d; i++){
			val = depth[idx_d];//qRgb(0,(depth[idx]>>8)&255,depth[idx]&255);
			rowData_depth[i] = val;
			rowData_ir[i] = ir[idx_d];
			idx_d++;
		}
	}

	frame = data->pass;

	//write the images.
	std::stringstream ss;
	ss << _folderName << "/ir_" << frame << ".png";;
	irImg.save(ss.str().c_str(), "PNG",100);
			
	std::stringstream ss2;
	ss2 << _folderName << "/depth_" << frame << ".png";
	depthImg.save(ss2.str().c_str(), "PNG");

	
	if(saveMetaData){
		std::ofstream file;
		file.open(std::string(_folderName + "/sensor.kin2"));
		file << "depth " << data->depth_w << " " << data->depth_h << "\n";
		file << "infrared " << data->depth_w << " " << data->depth_h << "\n";
		file << "focal " << data->f_x << " " << data->f_y << "\n";
		file << "principal " << data->principal_x << " " << data->principal_y << "\n";
		file << "distortion " << data->k2 << " " << data->k4 << " " << data->k6 << "\n";
		
		file.close();
	}
}


void fileWriteTask_raw::run(){
	if(dataMode == DEPTH_COLOR){
		storeDepthAndColor();
	}
	else if (dataMode == DEPTH_IR)
	{
		storeDepthAndIR();
	}

}