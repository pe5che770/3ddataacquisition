#include "myVAO.h"
#include "glDebugging.h"


myVAO::myVAO(void)
{
	glGenVertexArrays = NULL;
	glBindVertexArray = NULL;
	glDeleteVertexArrays = NULL;
	created = false;
}


myVAO::~myVAO(void)
{
	if(created){
		glDeleteVertexArrays(1, &vao);
		//glDebuggingStuff::didIDoWrong();
	}
}

void myVAO::create()
{
	if(!created){
		glGenVertexArrays = (GL_GEN_VARRAYS) wglGetProcAddress("glGenVertexArrays");
		glBindVertexArray = (GL_BIND_VARRAY) wglGetProcAddress("glBindVertexArray");
		glDeleteVertexArrays = (GL_DELETE_VARRAY) wglGetProcAddress("glDeleteVertexArrays");
		
		glGenVertexArrays(1, &vao);
		glDebugging::didIDoWrong();
		created = true;
	}

}

void myVAO::bind()
{
	if(!created){
		create();
	}
	glBindVertexArray(vao);
	glDebugging::didIDoWrong();
}

void myVAO::release()
{
	if(created){
		glBindVertexArray(0);
	}
	glDebugging::didIDoWrong();
}

bool myVAO::isCreated()
{
	return created;
}
