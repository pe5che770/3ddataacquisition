#pragma once
#include "Definitions.h"
#include <Windows.h>
#include "Sensors.h"
#include <Kinect.h>
#include "rawKinect2Data.h"

class QThread;
class kinect2Worker;
class cudaTool;

class kinect2RawDataProvider
{
public: 
	enum mode {COLOR_DEPTH, COLOR_IR};
private:
	mode dataMode;

	//std::vector<float> depthFloatBuffer, normalFloatBuffer;
	std::vector<UINT16> rawDepth; 
	std::vector<UINT16> rawIR; 
	std::vector<RGBQUAD> rawColor;
	std::vector<DepthSpacePoint> color2Depth; 
	std::vector<ColorSpacePoint> depth2Color;
	int kin2_pass;

	scanning::Cloud::Ptr cloud;
	
	int width, height; 
	int num_px_depth;
	
	//the sensor
	kinectV2Description kin_description;
	QThread * thread;
	kinect2Worker *worker;

	//put it into worker thread?
	cudaTool * cuda;
public:
	kinect2RawDataProvider(mode dataMode = COLOR_DEPTH);
	~kinect2RawDataProvider(void);

	virtual bool reloadData();
	virtual void recomputeClouds();

	RangeSensorDescription * getDescription(){
		return &kin_description;
	}

	void getRawBuffers(/*std::vector<UINT16> &depth,
	std::vector<RGBQUAD> &color, 
	std::vector<DepthSpacePoint> & c2d,
	std::vector<ColorSpacePoint> & d2c*/
	rawKinect2Data & data);

	virtual scanning::Cloud::Ptr & getPointCloud();
};

