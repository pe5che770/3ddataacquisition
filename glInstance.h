#pragma once
#include "glDisplayable.h"

class glInstance: public glDisplayable
{
private:
	glDisplayable * myInstance;
	QMatrix4x4 instance2World;

public:
	glInstance(glDisplayable * other);
	~glInstance(void);

	void mult(QMatrix4x4 & other);
	QMatrix4x4 getInstance2World();
	void setInstance2World(QMatrix4x4 & other);

	virtual void glPrepare() ;

	virtual bool isGlPrepared() ;

	virtual void glUpdate();

	virtual bool isGlUpToDate();

	virtual void glDraw();

	virtual QMatrix4x4 getModel2World();

	virtual QGLShaderProgram & get_m_shader();

};

