#pragma once
#include <Kinect.h>
#include <vector>

class rawKinect2Data
{
public:
	std::vector<UINT16> rawDepth; 
	std::vector<UINT16> rawIR; 
	std::vector<RGBQUAD> rawColor;
	std::vector<DepthSpacePoint> color2Depth; 
	std::vector<ColorSpacePoint> depth2Color;

	int pass;

	//intrinsics
	int depth_w, depth_h, color_w, color_h;
	float principal_x, principal_y, f_x, f_y;
	float k2, k4, k6;
public:
	rawKinect2Data(void){}
	~rawKinect2Data(void){}
};

