#include "kinect1Provider.h"
#include <iostream>
#include <fusion.h>
#include <NuiKinectFusionApi.h>
#include <assert.h>
#include <limits>
#include <omp.h>


//#include "kinectIntrinsics.h"
/*#include "KinectV1defs.h"
#include "kinectCastMacro.h"*/

kinect1Provider::kinect1Provider(providerType whatToLoad):
	kinectProvider(whatToLoad),
	myCloud(new scanning::Cloud()),
	filteredCloud(new scanning::Cloud())
{
	width = kin1_description.width(); height = kin1_description.height();
	data = new USHORT[width*height*4];


	myCloud->points.resize(width*height, scanning::Point());
	myCloud->width = width; // Image-like organized structure, with 640 rows and 480 columns,
	myCloud->height = height; // thus 640*480=307200 points total in the dataset
	filteredCloud->points.resize(width*height, scanning::Point());
	filteredCloud->width = width; 
	filteredCloud->height = height; 
	//colorData = new BYTE[width*height*4];

	colorCoordinates.resize(width*height * 2,0);
	colorCoordinates_float.resize(width*height * 3,0);
	rawFloatFrame.resize(width*height);
	//filteredFloatFrame.resize(width*height);
	colorData.resize(width*height*4,0);

	if(!initKinect()){
		std::cout << "\n***Failed to init kinect..\n";
	}
}


kinect1Provider::~kinect1Provider(void)
{
	/*if(sensor != NULL){
		sensor->NuiShutdown();
		sensor->Release();
	}*/
	delete[] data;
	//delete[] colorData;
}

void kinect1Provider::resolve(HRESULT & status)
{
	
	if(status == S_OK)
		std::cout << " s_ok";
	else if(status == E_FAIL)
		std::cout << "e_fail";
	else if(status == E_NUI_FRAME_NO_DATA)
		std::cout << "E_NUI_FRAME_NO_DATA";
	else if(status == E_INVALIDARG)
		std::cout << "E_INVALIDARG";
	else if(status == E_NUI_BADINDEX)
		std::cout << "E_NUI_BADINDEX";
	else if(status == E_NUI_DEVICE_NOT_CONNECTED)
		std::cout << "E_NUI_DEVICE_NOT_CONNECTED";
	else if(status == E_INVALIDARG)
		std::cout << "E_NUI_FEATURE_NOT_INITIALIZED";
	else if(status == E_INVALIDARG)
		std::cout << "E_NUI_FUSION_TRACKING_ERROR";
	else if(status == E_INVALIDARG)
		std::cout << "E_NUI_GPU_FAIL";
	else if(status == E_INVALIDARG)
		std::cout << "E_OUTOFMEMORY";
	else if(status == E_INVALIDARG)
		std::cout << "E_POINTER";
	else 
		std::cout << "unknown error: " <<status;

	std::cout << "\n";
}

bool kinect1Provider::initKinect()
{
	// Get a working kinect sensor
	int numSensors;
	HRESULT status;
	if (status = NuiGetSensorCount(&numSensors) < 0 || numSensors < 1){
		std::cout << "Error in init: no sensor found." ;
		resolve(status);
		return false;
	}
	else{
		std::cout << " found " << numSensors << "kinect sensors\n";
	}
	if (status = NuiCreateSensorByIndex(0, &sensor) < 0) {
		std::cout << "Error in init: could not create sensor." ;
		resolve(status);
		return false;
	}
	else{
		std::cout << "created sensor by index" << "\n";
	}


	// Initialize sensor
	status = sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX);//->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR);
	if(status != S_OK){
		std::cout << "error upon nuiinitialize";
		resolve(status);
	}
	status = sensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,                     // Depth camera or rgb camera?
        NUI_IMAGE_RESOLUTION_640x480,             // Image resolution
        0,   // Image stream flags, e.g. near mode
        2,      // Number of frames to buffer
        NULL,   // Event handle
        &depthStream);
	if(status != S_OK){
		std::cout << "error upon NuiImageStreamOpen";
		resolve(status);
	}
	
	status = sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, // Depth camera or rgb camera?
		NUI_IMAGE_RESOLUTION_640x480,    // Image resolution
		0,		// Image stream flags, e.g. near mode
		2,		// Number of frames to buffer
		NULL,   // Event handle
		&colorStream);//*/
	if(status != S_OK){
		std::cout << "error upon NuiImageStreamOpen";
		resolve(status);
	}
	return true;
}

bool kinect1Provider::reloadDepthData(INuiSensor* sensor, HANDLE & depthStream, int maxTries, int timeOut)
{
	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;
	HRESULT state=0;
	int tries = 0;
	while ((state = sensor->NuiImageStreamGetNextFrame(depthStream, timeOut , &imageFrame)) != S_OK){
		std::cout << "imgstreamGetNextFrame failed";
		//resolve(state);
		tries ++;
		if(tries > maxTries){
			std::cout << "\n*** imgstreamGetNextFrame failed in kinectStatistics\n";
			return false;
		}
	}

	INuiFrameTexture* texture = imageFrame.pFrameTexture;
    texture->LockRect(0, &LockedRect, NULL, 0);
	if (LockedRect.Pitch != 0)
    {
        const USHORT* curr = (const USHORT*) LockedRect.pBits;
        const USHORT* dataEnd = curr + (width*height);
		USHORT* dest = data;

        while (curr < dataEnd) {
            // Get depth in millimeters
            //USHORT depth = NuiDepthPixelToDepth(*curr++);

            //copy value.
            //*dest++ = depth;
			*dest++ = *curr++;
        }
    }
    texture->UnlockRect(0);
    sensor->NuiImageStreamReleaseFrame(depthStream, &imageFrame);/**/
	return true;
}

bool kinect1Provider::reloadColorData(INuiSensor* sensor, HANDLE & rgbStream, int maxTries, int timeOut)
{
	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;
	HRESULT state=0;
	int tries = 0;
	while ((state = sensor->NuiImageStreamGetNextFrame(rgbStream, /*timeOut*/0 , &imageFrame)) != S_OK){
		//std::cout << "color failed";
		//resolve(state);
		tries ++;
		if(tries > maxTries){
			std::cout << "\n*** color totally failed\n";
			return false;
		}
	}

	INuiFrameTexture* texture = imageFrame.pFrameTexture;
	texture->LockRect(0, &LockedRect, NULL, 0);
	if (LockedRect.Pitch != 0)
	{
		const BYTE* curr = (const BYTE*) LockedRect.pBits;
		const BYTE* dataEnd = curr + (width*height*4);
		BYTE* dest = &(colorData[0]);

		//memcpy???
		while (curr < dataEnd) {
			//copy value.
			*dest++ = *curr++;
		}
	}
	texture->UnlockRect(0);/**/
	sensor->NuiImageStreamReleaseFrame(rgbStream, &imageFrame);

	return true;
}

bool kinect1Provider::reloadColorData()
{
	return reloadColorData(sensor,colorStream, 30, 100);
}

bool kinect1Provider::reloadDepthData()
{
	return reloadDepthData(sensor, depthStream, 30,100);
}

bool kinect1Provider::reloadData(){
	bool newDepth = false;
	reloadColorData();
	newDepth = reloadDepthData();
	copyDepthData_meter(&rawFloatFrame[0]);
	
	reloadColorCoords();



	return newDepth;
}


HRESULT kinect1Provider::reloadColorCoords()
{
	HRESULT res = sensor->NuiImageGetColorPixelCoordinateFrameFromDepthPixelFrameAtResolution(
		NUI_IMAGE_RESOLUTION_640x480,
		NUI_IMAGE_RESOLUTION_640x480,
		width*height,
		data,
		width*height*2,
		&(colorCoordinates[0])
		);
	return res;
}



void kinect1Provider::copyDepthDataRGBA(GLubyte * dest)
{
	for(int i = 0; i < width * height; ++i ){
		for (int j = 0; j < 3; ++j)
			dest[i*4 +j] = (BYTE) data[i]%256;

		dest[i*4 +3] = 0xff;
	}
}

void kinect1Provider::copyDepthData_meter(float * dest)
{
	//FuFusionDepthProcessor t;
	for(int i = 0; i < width * height; ++i ){
		dest[i] = (0.f+ NuiDepthPixelToDepth(data[i]))/1000;
	}
}



RangeSensorDescription* kinect1Provider::getDescription(){
	return & kin1_description;
}

void kinect1Provider::recomputeClouds(){

	getCloudFromDepth(*myCloud, &rawFloatFrame[0], & kin1_description);
	
	int index=0, color_i, color_j, color_ind;
	auto & points = myCloud->points;
	auto & pointsFiltered = filteredCloud->points;

	float qnan = std::numeric_limits<float>::quiet_NaN();
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){
		//i = index/width;
		//j = index % width; 
		color_i = colorCoordinates[2*index];
		color_j = colorCoordinates[2*index+1];
		color_ind = color_i + color_j*width;


		points[index].r = (colorData[color_ind*4+2]);
		points[index].g = (colorData[color_ind*4+1]);
		points[index].b = (colorData[color_ind*4]);
		index++;
		}
	}
}

scanning::Cloud::Ptr & kinect1Provider::getPointCloud()
{
	return myCloud;
}


void kinect1Provider::getCloudFromDepth( scanning::Cloud & points, float * depth, RangeSensorDescription * desc )
{
	float z;
	int index = 0;
	float qnan = std::numeric_limits<float>::quiet_NaN();
	int height = desc->height();
	int width = desc->width();
	for(int i = 0; i < height; i++){
		for(int j = 0; j < width; j++){

		z = depth[index];
		desc->unproject(j,i,z,points[index].x,points[index].y);
		z = (z==0? qnan: z);
	/*	y = CAST_J2Y(i,z);
		points[index].x = x;
		points[index].y = y;*/
		points[index].z = z;
		points[index].r = 1;
		points[index].g = 0;
		points[index].b = 0;
		index++;
		}
	}
}


scanning::Cloud::Ptr & kinect1Provider::getNewPointCloud()
{
	
	reloadDepthData();
	reloadColorData();
	reloadColorCoords();
	return getPointCloud();
}


void kinect1Provider::getRawBuffers(std::vector<UINT16> &depths, std::vector<RGBQUAD> & colors){
	depths.resize(width*height);
	colors.resize(width * height);
	long color_i, color_j, color_ind;
	RGBQUAD tmp;
	for(int i = 0; i < width*height; i++){
		depths[i] = data[i];

		color_i = colorCoordinates[2*i];
		color_j = colorCoordinates[2*i+1];
		color_ind = color_i + color_j*width;
		tmp.rgbRed = colorData[color_ind*4+2];
		tmp.rgbGreen = colorData[color_ind*4+1];
		tmp.rgbBlue = colorData[color_ind*4];
		colors[i] = tmp; 
	}
}


	// Calculate correct XY scaling factor so that our vertices are correctly placed in the world
	// This helps us to unproject from the Kinect's depth camera back to a 3d world
	// Since the Horizontal and Vertical FOVs are proportional with the sensor's resolution along those axes
	// We only need to do this for horizontal
	// I.e. tan(horizontalFOV)/depthWidth == tan(verticalFOV)/depthHeight
	// Essentially we're computing the vector that light comes in on for a given pixel on the depth camera
	// We can then scale our x&y depth position by this and the depth to get how far along that vector we are
//	const float DegreesToRadians = 3.14159265359f / 180.0f;
//	m_xyScale = tanf(NUI_CAMERA_DEPTH_NOMINAL_HORIZONTAL_FOV * DegreesToRadians * 0.5f) / (m_depthWidth * 0.5f);    


BYTE * kinect1Provider::getColorData()
{
	return & (colorData[0]);
}

std::vector<float>& kinect1Provider::getXYToColorCoord()
{
	HRESULT res = reloadColorCoords();


	int idx=0;
	int i_j, tmp;
	for(int i = 0; i < width*height; i++){
		//(i,j)<-i/width,i%width
		//(j,i)<- i%width, i/width
		// <- i%width *height + i/width$
		i_j = i%width *height + i/width;
		 
		tmp = colorCoordinates[idx];
		colorCoordinates_float[3*i_j] =  (1.f * tmp)/width;
		idx++;
		tmp = colorCoordinates[idx];
		colorCoordinates_float[3*i_j+1] = (1.f * tmp)/height;
		idx++;
		colorCoordinates_float[3*i_j+2] =  0;
	}

	if(res != S_OK){
		resolve(res);
	}
	return colorCoordinates_float;
}
/*
void kinect1Provider::computeNormals(scanning::Cloud::Ptr & myCloud)
{
	int i, j;
	for(i=0; i< width; i++){
		for(j=0; j<height; j++){
			if(i < width -1 && j < height -1){
				myCloud->at(i,j).getNormalVector3fMap() =
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i+1,j).getVector3fMap()).cross(
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i,j+1).getVector3fMap()));
			}
			else if(i ==0){
				myCloud->at(i,j).getNormalVector3fMap() =
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i+1,j).getVector3fMap()).cross(
					(myCloud->at(i,j-1).getVector3fMap() - myCloud->at(i,j).getVector3fMap()));
			}
			else if (j == 0){
				myCloud->at(i,j).getNormalVector3fMap() =
					(myCloud->at(i-1,j).getVector3fMap() - myCloud->at(i,j).getVector3fMap()).cross(
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i,j+1).getVector3fMap()));
			}
			else{
				myCloud->at(i,j).getNormalVector3fMap() =
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i-1,j).getVector3fMap()).cross(
					(myCloud->at(i,j).getVector3fMap() - myCloud->at(i,j-1).getVector3fMap()));
			}
			myCloud->at(i,j).getNormalVector3fMap().normalize();
		}
	}
}

void kinect1Provider::bilateralFilter_cpu(
	scanning::Cloud::Ptr & myCloud,
	scanning::Cloud::Ptr & filteredCloud)
{
	//bilateral filter...
	int i_delta, j_delta;
	float x,y,z;
	float sigma_s = 2, sigma_d = 0.1f;
	float sigma_d_sqr = sigma_d*sigma_d;
	float exp_s[5] = {exp(-4/sigma_s/sigma_s),
		exp(-1/sigma_s/sigma_s), 1, 
		exp(-1/sigma_s/sigma_s),
		exp(-4/sigma_s/sigma_s)};
	float weightd[5] = {exp(-4/sigma_d/sigma_d),
		exp(-1/sigma_d/sigma_d), 1, 
		exp(-1/sigma_d/sigma_d),
		exp(-4/sigma_d/sigma_d)};
	float w_p,w;

//#pragma omp parallel for private(i_delta,j_delta, w_p,w)
	for(int i = 0; i < width; i++){
		for(int j = 0; j < height; j++){
			//i = index/width;
			//j = index % width; 
			w_p = x= y = z= 0;
			auto & p = myCloud->at(i,j);
			auto & target = filteredCloud->at(i,j);
			target.x= target.y=target.z=0;
			for(i_delta = -2;i_delta<=2; i_delta++){
				for(j_delta = -2;j_delta<=2; j_delta++){
					if(i+i_delta < 0 || i+ i_delta >= width || j + j_delta < 0 || j + j_delta >= height){
						continue;
					}
					//std::cout << "  " << i << "," << j;
					auto & q = myCloud->at(i+i_delta, j + j_delta);
					w = exp_s[2+i_delta] * exp_s[2+j_delta] *
						exp(
						(p.z - q.z)*
						(p.z - q.z)/sigma_d_sqr);
					w_p += w;
					
					target.getVector3fMap() += q.getVector3fMap()*w;
				}
			}
			target.getVector3fMap()/=w_p;
		}
	}
}*/
