#pragma once

//storing pngs
#include <Windows.h>
#include <qrunnable.h>
#include <string>
#include <vector>


class RangeSensorDescription;
class rawKinect2Data;

class fileWriteTask:public QRunnable{
	std::string _folderName;
	int _width, _height;
	int frame;
public:
	std::vector<RGBQUAD> color;
	std::vector<UINT16> depth;

	fileWriteTask(std::string name, RangeSensorDescription * sensor, int frm);

	~fileWriteTask();

	void run();
};


class fileWriteTask_raw:public QRunnable{
	std::string _folderName;
	int frame;
	bool saveMetaData;
public:
	enum mode {DEPTH_COLOR, DEPTH_IR};

	rawKinect2Data * data;
	mode dataMode;

	fileWriteTask_raw(std::string name, int frame, bool saveMetaData, mode m = DEPTH_COLOR);

	~fileWriteTask_raw();

	void run();

private:
	void storeDepthAndColor();
	void storeDepthAndIR();
};
