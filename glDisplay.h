#pragma once
//#include "gl3w.h"
#include <QtOpenGL/QGLWidget>
#include <QtOpenGL/QGLFormat>
#include <QMatrix4x4>

#include "sceneManager.h"
/*#include <QOpenGLDebugLogger>
#include <QTimer>*/
#include "trackBallListener.h"

class glDisplay:public QGLWidget
{
	Q_OBJECT
private:
	QMatrix4x4 proj;
	sceneManager scene;
	/*QOpenGLDebugLogger * logger;
	QTimer * timer;*/
	trackBallListener * trackBall;
public:
	glDisplay(QGLFormat & format, QWidget *parent, int w, int h);
	~glDisplay(void);

	virtual void mouseMoveEvent ( QMouseEvent * event );
	virtual void mousePressEvent ( QMouseEvent * event );
	virtual void wheelEvent ( QWheelEvent * event );

	sceneManager * getSceneManager();

	void setLineWidth(float w);
	void setPointWidth(float w);

public Q_SLOTS:
	//void onMessageLogged(QOpenGLDebugMessage m);
	void refresh();

protected:
	void initializeGL();
	void resizeGL(int width, int height);
	void paintGL();
	
};

