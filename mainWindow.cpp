#include "mainWindow.h"
#include <iostream>
mainWindow::mainWindow(QGLFormat & format)
{
	myGlDisp = new glDisplay(format, this, 400, 400);
	
	this->setUpButtons();
	this->layoutGui();
	this->addAction();

	this->setWindowTitle("Recording App");
	this->resize(600, 500);
	this->show();
}


mainWindow::~mainWindow(void)
{
}


void mainWindow::layoutGui()
{
	QVBoxLayout *buttons = new QVBoxLayout();
	buttons->addWidget(l_out);
	buttons->addWidget(b1);
	buttons->addWidget(b2);
	buttons->addWidget(b3);
	buttons->addWidget(b4);


	QHBoxLayout *mainLayout = new QHBoxLayout();
	mainLayout->addWidget(myGlDisp,1);
	mainLayout->addLayout(buttons);

	QWidget * mainWidget = new QWidget();
	mainWidget->setLayout(mainLayout);
	this->setCentralWidget(mainWidget);
}

void mainWindow::addAction()
{
	
}


sceneManager * mainWindow::getSceneManager()
{
	return myGlDisp->getSceneManager();
}

 glDisplay* mainWindow::getGLWidget()
{
	return myGlDisp;
}

 void mainWindow::setUpButtons()
 {
	b1 = new QPushButton(this);
	b1->setText("Action1");
	b2 = new QPushButton(this);
	b2->setText("Action2");
	b3 = new QPushButton(this);
	b3->setText("Action3");
	b4 = new QPushButton(this);
	b4->setText("Action4");

	l_out = new QLabel(this);
	l_out->setText("      ");
 }


 void mainWindow::printClicked()
 {
	 std::cout << "clicked";
 }

 QPushButton * mainWindow::getButton1()
 {
	 return b1;
 }

 QPushButton * mainWindow::getButton2()
 {
	 return b2;
 }

 QPushButton * mainWindow::getButton4()
 {
	 return b4;
 }

 QPushButton * mainWindow::getButton3()
 {
	 return b3;
 }


 void mainWindow::keyPressEvent(QKeyEvent* e){
	 l_out->setText(e->text());
	 bool ok;
	 int val = e->text().toInt(&ok);
	 if(ok){
		 Q_EMIT(numericalKeyPressed(val));
	 }
	 else{
		switch (e->text().at(0).toAscii())
		{
		case 'q':
			Q_EMIT(numericalKeyPressed(10));
			break;
		case 'w':
			Q_EMIT(numericalKeyPressed(11));
			break;
		case 'e':
			Q_EMIT(numericalKeyPressed(12));
			break;
		case 'r':
			Q_EMIT(numericalKeyPressed(13));
			break;
		case 't':
			Q_EMIT(numericalKeyPressed(14));
			break;
		case 'z':
			Q_EMIT(numericalKeyPressed(15));
			break;
		case 'u':
			Q_EMIT(numericalKeyPressed(16));
			break;
		case 'i':
			Q_EMIT(numericalKeyPressed(17));
			break;
		case 'o':
			Q_EMIT(numericalKeyPressed(18));
			break;
		case 'p':
			Q_EMIT(numericalKeyPressed(19));
			break;
		}
		//Q_EMIT(charKeyPressed(e->text().at(0).toAscii()));
	 }
 }

 void mainWindow::printInfo(std::string info){
	 QString text(info.c_str());
	 l_out->setText(text);
 }