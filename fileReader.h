#pragma once
#include "Definitions.h"
#include <string>
#include <vector>


class fileReader
{
public:
	fileReader(std::string folder, std::string basedepth = "depth_" , std::string baseimg = "img_");
	~fileReader(void);

	int numberOfFrames();
	void readFrame(int i, std::vector<scanning::UINT16> & depth, std::vector<scanning::RGBQUAD> & target_color);
	void readFrame(int i, std::vector<scanning::UINT16> & depth, std::vector<scanning::UINT16> & target_ir);
	int frameWidth();
	int frameHeight();
	static void listFiles(std::string folder, std::string query, std::vector<std::string> &target);


private:
	std::vector<std::string> files_depth;
	std::vector<std::string> files_color;
	std::string _folder;
	int _width, _height;
};

