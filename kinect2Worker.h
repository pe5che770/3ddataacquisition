#pragma once
#include <qobject.h>
#include <Kinect.h>
#include <vector>
#include <qmutex.h>


//fwd dec
class RangeSensorDescription;

/*
* worker, to be run in his own qthread, which waits for the kinect v2 
* to signal that new data is ready. It will then copy the data and
* use the kinect api to align color and depth.
* The data can then be safely polled with the getData method.
*
*/
class kinect2Worker:public QObject
{
	Q_OBJECT
public:
	enum dataMode {DEPTH_COLOR, DEPTH_IR};
private:
	//the sensor.
	IKinectSensor * m_pKinectSensor;
	IMultiSourceFrameReader * m_pMultiReader;
	ICoordinateMapper *m_pCoordMapper;
	

	IDepthFrameReader * m_pDepthReader;
	IColorFrameReader * m_pColorReader;
	ICoordinateMapper * m_pMapper;


	dataMode mode;

	WAITABLE_HANDLE _hDephtEvent;
	HANDLE _hstopEvent, _hDataWasUpdatedEvent;

	
	//intrinsic coordinates for depth:
	bool intrinsicsLoaded, depth2CamTableLoaded;

	int		width,			height,			num_px_depth;
	float	focalLength_x,	focalLength_y, 
			principal_x,	principal_y, 
			k_distortion_2, k_distortion_4, k_distortion_6;
	std::vector<PointF> depth2CamTable;

	//intrinsics for color:
	int color_width, color_height, num_px_color;

	//FPS
	int pass, startTime;

	//The buffers
	QMutex bufferAccess;
	std::vector<UINT16> depthBuffer;
	std::vector<UINT16> infraredBuffer;
	std::vector<RGBQUAD> colorBuffer;
	//stores for each depth value the colorspace coordinate.
	std::vector<ColorSpacePoint> depth2Color;
	std::vector<DepthSpacePoint> color2Depth;
	std::vector<UINT16> depthAtColorPos;

public:
	kinect2Worker(RangeSensorDescription * description, dataMode mode = DEPTH_COLOR);
	~kinect2Worker(void);

	void shutDownGracefully();
	bool getData(std::vector<float> & target,
		std::vector<UINT16> & target_rawDepth,
		std::vector<RGBQUAD> & target_color);

	bool getRawData(std::vector<UINT16> & rawDepth, std::vector<RGBQUAD> & rawColor, 
		std::vector<DepthSpacePoint> & color2Depth, std::vector<ColorSpacePoint> & depth2Color,
		int & pass);

	bool getRawData_DEPTH_IR(std::vector<UINT16> & rawDepth, std::vector<UINT16> & rawIR, 
		int & pass);

	bool getDepth2CamTable(){
		if(!depth2CamTableLoaded){
			return false;
		}

		return depth2CamTableLoaded;
	}

	bool getAllIntrinsics(
		int & depth_w, int & depth_h,
		int & color_w, int & color_h,
		float & f_x, float & f_y,
		float & princ_x, float & princ_y,
		float & k_2, float & k_4, float & k_6){

		if(intrinsicsLoaded ){
			depth_w = width;
			depth_h = height;
			color_w = color_width;
			color_h = color_height;
			f_x = focalLength_x;
			f_y = focalLength_y;
			princ_x = principal_x;
			princ_y = principal_y;
			k_2 = k_distortion_2;
			k_4 = k_distortion_4;
			k_6 = k_distortion_6;
			return true;
		}
		return false;
	}

	bool getIntrinsics(float * f_x, 
		float * f_y, 
		float * p_x, 
		float * p_y, 
		float * k2, 
		float * k4,
		float * k_6);

	bool areIntrinsicsLoaded(){return intrinsicsLoaded;};

private:
	void initKinect();
	void initIntrinsics();
	bool resolve(HRESULT &hr);

	void mainLoop_depth_color();
	void mainLoop_depth_IR();

public Q_SLOTS:
	void mainLoop();
Q_SIGNALS:
	void finished();
};

