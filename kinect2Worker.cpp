#include "kinect2Worker.h"
#include <iostream>
#include "Sensors.h"
#include <cmath>

kinect2Worker::kinect2Worker(RangeSensorDescription * description, dataMode m)
{
	mode = m;
	m_pKinectSensor = NULL;

	m_pMultiReader = NULL;
	m_pCoordMapper = NULL;

	m_pDepthReader= NULL;
	m_pColorReader= NULL;

	//this event does not reset automatically
	_hstopEvent = CreateEvent(NULL,true,false, NULL);
	//this event resets automatically after having triggered one thread.
	_hDataWasUpdatedEvent = CreateEvent(NULL,false,false, NULL);

	color_width = color_height = -1;
	width = description->width();
	height = description->height();
	num_px_depth = width*height;

	depth2CamTableLoaded = intrinsicsLoaded = false;
	k_distortion_2 = k_distortion_4 = k_distortion_6 = 0;

	
}


kinect2Worker::~kinect2Worker(void)
{
	if(	m_pKinectSensor != NULL){
		m_pKinectSensor->Close();
		m_pKinectSensor->Release();
	}
	if(m_pDepthReader!= NULL){
		m_pDepthReader->Release();
	}
	if(m_pColorReader!= NULL){
		m_pColorReader->Release();
	}
	if(m_pMultiReader != NULL){
		m_pMultiReader->Release();
	}
	if(m_pCoordMapper != NULL){
		m_pCoordMapper->Release();
	}

	std::cout << "deleted kinect2 Worker\n";

	CloseHandle(_hstopEvent);
	CloseHandle(_hDataWasUpdatedEvent);
}


bool kinect2Worker::resolve(HRESULT & hr){
	if(!SUCCEEDED(hr)){
		std::cout <<"**Error in kinext2Worker: HRESULT " << hr << ": ";
		if(hr == E_PENDING){
			std::cout <<"Frame pending...";
		}
		std::cout << "\n";
		return false;
	}
	return true;
}

void kinect2Worker::initKinect(){
	HRESULT hr;

	std::cout << "initializing Kinect 2\n";
	hr = GetDefaultKinectSensor(&m_pKinectSensor);
	if(!resolve(hr)){
		return;
	}

	//open Sensor
	hr = m_pKinectSensor->Open();
	if(!resolve(hr)){
		return;
	}

	if(mode == DEPTH_COLOR){
		//open multiframeReader
		hr = m_pKinectSensor->OpenMultiSourceFrameReader(FrameSourceTypes_Color|FrameSourceTypes_Depth, &m_pMultiReader );
		if(resolve(hr)){
			m_pMultiReader->SubscribeMultiSourceFrameArrived(&this->_hDephtEvent);
		}
		if (resolve(hr)){
			std::cout << "initialized kinect v2";
		}

		//setup buffer
		depthBuffer.resize(num_px_depth,0);
		depth2Color.resize(num_px_depth);
		color2Depth.resize(num_px_depth);
		depth2CamTable.resize(num_px_depth);
		this->m_pKinectSensor->get_CoordinateMapper(&m_pCoordMapper);
	}
	else if(mode == DEPTH_IR){
		hr = m_pKinectSensor->OpenMultiSourceFrameReader(FrameSourceTypes_Infrared|FrameSourceTypes_Depth, &m_pMultiReader );
		if(resolve(hr)){
			m_pMultiReader->SubscribeMultiSourceFrameArrived(&this->_hDephtEvent);
		}
		if (resolve(hr)){
			std::cout << "initialized kinect v2";
		}

		//setup buffer
		depthBuffer.resize(num_px_depth,0);
		infraredBuffer.resize(num_px_depth,0);
		//mapper to get intrinsics
		this->m_pKinectSensor->get_CoordinateMapper(&m_pCoordMapper);
	}
}

void kinect2Worker::shutDownGracefully(){

	//singal stopping
	SetEvent(_hstopEvent);
	//this will trigger a qt finish event, which again will trigger 
	//qthread quit and deleteLaterEvents.
}


bool kinect2Worker::getRawData(
	std::vector<UINT16> & rawDepth, std::vector<RGBQUAD> & rawColor, 
	std::vector<DepthSpacePoint> & color2Depth_tgt, std::vector<ColorSpacePoint> & depth2Color_tgt, int & _pass)
{
	if(mode!= DEPTH_COLOR){
		throw std::runtime_error("wrong data mode selected in kinect2Worker");
	}
	bool gotData = false;
	switch(WaitForSingleObject(_hDataWasUpdatedEvent, 500))
	{
	case WAIT_OBJECT_0:
		{
			gotData = true;
			//std::cout << "Acquiring raw data ...\n";
			QMutexLocker lock(&bufferAccess);
			rawDepth = depthBuffer;
			rawColor = colorBuffer;
			color2Depth_tgt = color2Depth;
			depth2Color_tgt =  depth2Color;
			_pass = pass;
		}
	}

	if(gotData && !intrinsicsLoaded){
		initIntrinsics();
	}
	return gotData;
}

bool kinect2Worker::getRawData_DEPTH_IR(std::vector<UINT16> & rawDepth,std::vector<UINT16> & rawIR, int & _pass)
{
	if(mode!= DEPTH_IR){
		throw std::runtime_error("wrong data mode selected in kinect2Worker");
	}
	bool gotData = false;
	switch(WaitForSingleObject(_hDataWasUpdatedEvent, 15000))
	{
	case WAIT_OBJECT_0:
		{
			gotData = true;
			//std::cout << "Acquiring raw data ...\n";
			QMutexLocker lock(&bufferAccess);
			rawDepth = depthBuffer;
			rawIR = infraredBuffer;
			_pass = pass;
		}
	}

	if(gotData && !intrinsicsLoaded){
		initIntrinsics();
	}
	return gotData;

}

bool kinect2Worker::getData(std::vector<float> & target, std::vector<UINT16> & target_rawDepth, std::vector<RGBQUAD> & target_color){
	
	if(mode!= DEPTH_COLOR){
		throw std::runtime_error("wrong data mode selected in kinect2Worker");
	}
	bool gotData = false;
	UINT16 oldDepth;
	switch(WaitForSingleObject(_hDataWasUpdatedEvent, 100))
	{
	case WAIT_OBJECT_0:
		{
			gotData = true;
			RGBQUAD zero = {0,0,0,1};
			DepthSpacePoint minOne = {-1,-1};
			int color_x, color_y;
			//int depth_x, depth_y;

			target_rawDepth = depthBuffer;
			target.resize(width * height, 0);
			target_color.resize(width*height);
						
			depthAtColorPos.resize(color_width/2 * color_height/2);
			depthAtColorPos.assign(depthAtColorPos.size(), 32000);

			//std::cout << "\t\t\consumer: buffer \n";
			QMutexLocker lock(&bufferAccess);
			//std::cout << "\t\tconsumer gotit\n";
			for(int i = 0; i < width*height; i++){
				target[i] = depthBuffer[i] / 1000.f;

				//store the minimal depth for each color pixel
				color_x = (int)(floor(depth2Color[i].X+ 0.5));
				color_y = (int)(floor(depth2Color[i].Y+ 0.5));
				if(0<= color_x && color_x <color_width
					&& 0 <= color_y && color_y < color_height){
					oldDepth = depthAtColorPos[color_y/3*color_width/3 + color_x/3];
					if(depthBuffer[i] < oldDepth){
						depthAtColorPos[color_y/3*color_width/3 + color_x/3] = depthBuffer[i];
					}
				}

				target_color[i] = zero;

			}
			for(int i = 0; i < width*height; i++){
				color_x = (int)(floor(depth2Color[i].X+ 0.5));
				color_y = (int)(floor(depth2Color[i].Y+ 0.5));

				if(0<= color_x && color_x <color_width
					&& 0 <= color_y && color_y < color_height){
					if(depthBuffer[i]- depthAtColorPos[color_y/3*color_width/3 + color_x/3] < 5){
						target_color[i] = colorBuffer[color_x + color_y*color_width];
					}
					else{
						target_color[i] = zero;
					}
				}
			}
		}
	}


	if(gotData && !intrinsicsLoaded){
		initIntrinsics();
	}
	return gotData;
}

void kinect2Worker::initIntrinsics(){
	CameraIntrinsics intrinsics;
	m_pCoordMapper->GetDepthCameraIntrinsics(&intrinsics);
	std::cout << "W = " << width << " H = " << height << "\n";
	focalLength_x =		intrinsics.FocalLengthX;
	focalLength_y =		intrinsics.FocalLengthY;
	principal_x =		intrinsics.PrincipalPointX;
	principal_y =		intrinsics.PrincipalPointY;
	k_distortion_2 =	intrinsics.RadialDistortionSecondOrder;
	k_distortion_4 =	intrinsics.RadialDistortionFourthOrder;
	k_distortion_6 =	intrinsics.RadialDistortionSixthOrder;

	std::cout << "Cam intrinsics: focal length:("
		<<intrinsics.FocalLengthX << "," << intrinsics.FocalLengthY << ") \ncenter:"
		<<intrinsics.PrincipalPointX << " ," << intrinsics.PrincipalPointY 
		<<"\n radial distortion : " << intrinsics.RadialDistortionSecondOrder << ", " 
		<< intrinsics.RadialDistortionFourthOrder << ", " << intrinsics.RadialDistortionSixthOrder << "\n";

	if(mode == DEPTH_COLOR){
		UINT32 npix = num_px_depth;
		PointF * pf = &depth2CamTable[0];
		m_pCoordMapper->GetDepthFrameToCameraSpaceTable(& npix, &pf);
	}
	
	intrinsicsLoaded = (focalLength_x != 0);

	//std::cout << "bastard table entry 0 " << depth2CamTable[0].X << "," << depth2CamTable[0].Y;
}

bool kinect2Worker::getIntrinsics(float * f_x, 
		float * f_y, 
		float * p_x, 
		float * p_y, 
		float * k2, 
		float * k4,
		float * k6){
	if(!intrinsicsLoaded){
		return false;
	}
	else{
		*f_x = focalLength_x; *f_y = focalLength_y; *p_x = principal_x; *p_y = principal_y;
		*k2 = k_distortion_2; *k4 = k_distortion_4; *k6 = k_distortion_6;
	}

}



void kinect2Worker::mainLoop_depth_color()
{
	pass = 0;
	startTime = GetTickCount();
	bool looping = true;
	HRESULT hres;
	while(looping){
		HANDLE handles[] = {reinterpret_cast<HANDLE>(this->_hDephtEvent), _hstopEvent};


		switch(WaitForMultipleObjects(_countof(handles), handles, false, 1000))
		{
		case WAIT_OBJECT_0:
			{


				IMultiSourceFrameArrivedEventArgs * pArgs = nullptr;
				hres = this->m_pMultiReader->GetMultiSourceFrameArrivedEventData(this->_hDephtEvent, &pArgs);
				resolve(hres);

				IMultiSourceFrameReference *pFrameReference = nullptr;
				hres = pArgs->get_FrameReference(&pFrameReference);
				resolve(hres);

				IMultiSourceFrame * pFrame = nullptr;

				IDepthFrameReference * pDepthFrameReference;
				IDepthFrame *pDepthFrame = nullptr;

				IColorFrameReference *pColorFrameReference = nullptr;
				IColorFrame *pColorFrame = nullptr;

				

				bool gotSome=false;
				/////////////////
				//get the reference to the synced frames.
				////////////////
				hres = pFrameReference->AcquireFrame(&pFrame);
				if(resolve(hres)){
					hres = pFrame->get_DepthFrameReference(&pDepthFrameReference);
					resolve(hres);

					//std::cout << "\t\t\tworker: buffer \n";
					{QMutexLocker lock(&bufferAccess);
					//std::cout << "\t\t\tworker: gotit \n";

					////////////////
					//get color Data
					////////////////
					hres = pFrame->get_ColorFrameReference(&pColorFrameReference);
					resolve(hres);
					if(SUCCEEDED(pColorFrameReference->AcquireFrame(&pColorFrame))){
																		
						//acquire colorframe meta data
						IFrameDescription * colorFrameDescription = nullptr;
						pColorFrame->get_FrameDescription(&colorFrameDescription);
						colorFrameDescription->get_Height(&color_height);
						colorFrameDescription->get_Width(&color_width);
						num_px_color = color_height * color_width;
						colorFrameDescription->Release();

						colorBuffer.resize(color_height*color_width);
						color2Depth.resize(color_height*color_width);

						//get the color data-
						UINT szInBytes = colorBuffer.size() * sizeof(RGBQUAD);
						RGBQUAD * pColorBuffer = &colorBuffer[0];
						pColorFrame->CopyConvertedFrameDataToArray(szInBytes, 
							reinterpret_cast<BYTE*>(pColorBuffer), 
							ColorImageFormat_Rgba);
						pColorFrame->Release();					
												
					}
					else{
						std::cout << "**error in kinectWorker2: color frame could not be acquired";
					}



					/////////////////
					//get depth data
					////////////////
					if(SUCCEEDED(pDepthFrameReference->AcquireFrame(&pDepthFrame))){

						pDepthFrame->CopyFrameDataToArray(num_px_depth, &depthBuffer[0]);
						pDepthFrame->Release();
						
						m_pCoordMapper->MapDepthFrameToColorSpace(num_px_depth,&depthBuffer[0], num_px_depth, &depth2Color[0]);
																	
						//go brag about it.
						gotSome = true;
						
						//get a map color-> depth
						m_pCoordMapper->MapColorFrameToDepthSpace(num_px_depth,&depthBuffer[0], num_px_color, &color2Depth[0]);
						
						//fps
						pass++;
					}
					else{
						std::cout << "**error in kinectWorker2: depth frame could not be acquired";
					}
					//cleanup depthframe
					pDepthFrameReference->Release();

					
					}//release Lock


					if(gotSome)	SetEvent(_hDataWasUpdatedEvent);
					//clean up color Frame
					pColorFrameReference->Release();

					//cleanup multiframe
					pFrame->Release();
				}
				pFrameReference->Release();//*/
				pArgs->Release();

				//fps
				if(pass%20 == 0){
					std::cout << "FPS kinect2Worker: \t" << 20 * 1000.f/(GetTickCount() - startTime)<< "\n";
					//pass = 0; 
					startTime = GetTickCount();
				}
							
				break;

			}
		case WAIT_OBJECT_0 + 1:
			{
				std::cout << "\n exiting the kinect2Worker Loop\n";
				looping = false;
				Q_EMIT(finished());
				break;
			}
		case WAIT_FAILED:
			{
				std::cout << "wait failed, exiting main loop.";
				looping = false;
				break;
			}
		}
	}
}

void kinect2Worker::mainLoop_depth_IR()
{
	pass = 0;
	startTime = GetTickCount();
	bool looping = true;
	HRESULT hres;
	while(looping){
		HANDLE handles[] = {reinterpret_cast<HANDLE>(this->_hDephtEvent), _hstopEvent};
		switch(WaitForMultipleObjects(_countof(handles), handles, false, 1000))
		{
		case WAIT_OBJECT_0:
			{
				IMultiSourceFrameArrivedEventArgs * pArgs = nullptr;
				hres = this->m_pMultiReader->GetMultiSourceFrameArrivedEventData(this->_hDephtEvent, &pArgs);
				resolve(hres);

				IMultiSourceFrameReference *pFrameReference = nullptr;
				hres = pArgs->get_FrameReference(&pFrameReference);
				resolve(hres);

				IMultiSourceFrame * pFrame = nullptr;
				IDepthFrameReference * pDepthFrameReference;
				IDepthFrame *pDepthFrame = nullptr;

				IInfraredFrameReference *pIRFrameReference = nullptr;
				IInfraredFrame *pIRFrame = nullptr;

				

				bool gotSome=false;
				/////////////////
				//get the reference to the synced frames.
				////////////////
				hres = pFrameReference->AcquireFrame(&pFrame);
				if(resolve(hres)){
					hres = pFrame->get_DepthFrameReference(&pDepthFrameReference);
					resolve(hres);

					{QMutexLocker lock(&bufferAccess);
					

					////////////////
					//get IR Data
					////////////////
					hres = pFrame->get_InfraredFrameReference(&pIRFrameReference);
					resolve(hres);
					if(SUCCEEDED(pIRFrameReference->AcquireFrame(&pIRFrame))){
						
						//get the color data-
						UINT szInBytes = infraredBuffer.size() * sizeof(UINT16); //what might the size of that be...?
						UINT16 * pIrBuffer = &infraredBuffer[0];
						pIRFrame->CopyFrameDataToArray(num_px_depth, &infraredBuffer[0]);
						pIRFrame->Release();			

												
					}
					else{
						std::cout << "**error in kinectWorker2: color frame could not be acquired";
					}

					pIRFrameReference->Release();


					/////////////////
					//get depth data
					////////////////
					if(SUCCEEDED(pDepthFrameReference->AcquireFrame(&pDepthFrame))){

						pDepthFrame->CopyFrameDataToArray(num_px_depth, &depthBuffer[0]);
						pDepthFrame->Release();
						
						//go brag about it.
						gotSome = true;
						
						//fps
						pass++;
					}
					else{
						std::cout << "**error in kinectWorker2: depth frame could not be acquired";
					}
					//cleanup depthframe
					pDepthFrameReference->Release();

					
					}//release Lock
					if(gotSome)	SetEvent(_hDataWasUpdatedEvent);
					//cleanup multiframe
					pFrame->Release();
				}
				pFrameReference->Release();//*/
				pArgs->Release();

				//fps
				if(pass%20 == 0){
					std::cout << "FPS kinect2Worker: \t" << 20 * 1000.f/(GetTickCount() - startTime)<< "\n";
					//pass = 0; 
					startTime = GetTickCount();
				}
							
				break;

			}
		case WAIT_OBJECT_0 + 1:
			{
				std::cout << "\n exiting the kinect2Worker Loop\n";
				looping = false;
				Q_EMIT(finished());
				break;
			}
		case WAIT_FAILED:
			{
				std::cout << "wait failed, exiting main loop.";
				looping = false;
				break;
			}
		}
	}
}


void kinect2Worker::mainLoop(){

	initKinect();
	if(mode == DEPTH_COLOR){
		mainLoop_depth_color();
	}
	if(mode == DEPTH_IR)
	{
		mainLoop_depth_IR();
	}
}