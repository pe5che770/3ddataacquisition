#pragma once
#include "Definitions.h"
#include <Windows.h>
#include "kinectProvider.h"
#include <qmatrix4x4.h>
#include "Sensors.h"

class QThread;
class kinect2Worker;
class cudaTool;

class kinect2EventDrivenProvider:public kinectProvider
{
private:
	//buffers
	std::vector<float> depthFloatBuffer, normalFloatBuffer;
	std::vector<RGBQUAD> colorBuffer;
	std::vector<UINT16> depthBuffer; 

	scanning::Cloud::Ptr cloud;
	scanning::Cloud::Ptr filteredCloud;

	int width, height; 
	int num_px_depth;
	
	//the sensor
	kinectV2Description kin_description;
	QThread * thread;
	kinect2Worker *worker;

public:
	kinect2EventDrivenProvider(providerType whatToLoad = DEPTH_ONLY);
	~kinect2EventDrivenProvider(void);

	virtual bool reloadData();
	virtual void recomputeClouds();

	RangeSensorDescription * getDescription(){
		return &kin_description;
	}
	void getRawBuffers(std::vector<UINT16> & depth, 
		std::vector<RGBQUAD> & alignedColor){
			alignedColor = colorBuffer;
			depth = depthBuffer;
	}

	virtual scanning::Cloud::Ptr  & getPointCloud();
};

