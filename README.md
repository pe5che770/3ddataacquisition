# README #

### What is this repository for? ###

Simple code to record stuff with range sensors.

Comes as a cmake project, but will need slight customization to get it running on a machine.
Will work mostly on windows.

For asus xtion the auto exposure feature (leading to varrying colors over time) is currently hardcoded as off.

### Notes on the Dependencies and cmake ###

Dependencies are 

+ QT 4.8
+ Kinect SDK 1
+ Kinect SDK 2
+ OpenNI 2
+ boost
+ Opencv (can safely be kicked out of the cmake file)

some Dlls might have to be copied manually/made available on the PATH.

### How to install the project dependencies on windows

+ Install [Boost](https://gist.github.com/simplay/971ae93a70f1176b5ce9)
+ Make sure the boost version is compiled with the right compiler (on windows) cmake won't find boost if the version doesn't match, as the lib fies have the version number in them.
+ If boost is not installed to a std location, add the BOOST_ROOT to cmake. To find out where cmake goes wrong, if it doesnt find boost, add the variable Boost_DEBUG 1 (true).
+ Last but not least: if you want to link to the static boost libraries (and compiled those, their names start with the string libboost) add the variable Boost_USE_STATIC_LIBS 1 (true). 
+ Need to change hardcoded x64 to x86 in cmake files for kinects appropriately. Should make this automatic

### TODOs & Warnings
* The output formats to store the recorded sequences need to be customized.
* The main method should be customized to allow decent command line usage.
* The asus xtion cameras resolutions (as all others) are currently hardcoded. It might be good to extend the internal calibration representation / sensors to allow the use of other resolutions. E.g. the asus color sensor would allow twice the currently hardcoded color resolution.
* Maybe better calibrations need to be found. This is not relevant for the recording, but be aware that the calibrations used internally 
for visualization need not be very good and not fit for more general use. 
* There is possible Filereading (can be useful to look at the recorded scenes) but it should be adapted to teh output format of choice.