#include "Definitions.h"
#include "kinect2RawDataProvider.h"
#include <qmutex.h>
#include <qthread.h>
#include "kinect2Worker.h"

#include "Sensors.h"

kinect2RawDataProvider::kinect2RawDataProvider(mode m):
cloud(new scanning::Cloud()),
	dataMode(m)
{
	
	width = kin_description.width(); //WIDTH
	height = kin_description.height(); //HEIGHT
	num_px_depth = width * height;
	

	//depthFloatBuffer.resize(num_px_depth, 0);
	//normalFloatBuffer.resize(num_px_depth*3, 0);
	rawColor.resize(num_px_depth*4);
	rawDepth.resize(num_px_depth);
	color2Depth.resize(num_px_depth*4);
	depth2Color.resize(num_px_depth);

	cloud->resize(width * height);
	cloud->height = height;
	cloud->width = width;


	//Set up the worker which will get the data in its own thread.
	//connect signals such that deletion will work properly.
	thread = new QThread();
	if(dataMode == COLOR_DEPTH){
		worker = new kinect2Worker(&kin_description);
	}
	else if(dataMode == COLOR_IR){
		worker = new kinect2Worker(&kin_description, kinect2Worker::DEPTH_IR);
	}
	worker->moveToThread(thread);
	
	QObject::connect(thread, SIGNAL(started()), worker, SLOT(mainLoop()));
	QObject::connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
	QObject::connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start(QThread::TimeCriticalPriority);
}


kinect2RawDataProvider::~kinect2RawDataProvider(void)
{
	//worker: stop. This will trigger finished() and then the deleteLater()
	//such that both worker and thread are disposed gracefully.
	worker->shutDownGracefully();
}

void kinect2RawDataProvider::getRawBuffers(rawKinect2Data & data){
	data.rawDepth = rawDepth;
	data.rawColor = rawColor;
	data.color2Depth = color2Depth;
	data.depth2Color = depth2Color;
	data.pass = kin2_pass;
	data.rawIR = rawIR;
	worker->getAllIntrinsics(data.depth_w, data.depth_h, 
		data.color_w, data.color_h, 
		data.f_x, data.f_y, 
		data.principal_x, data.principal_y, 
		data.k2, data.k4, data.k6);
}

bool kinect2RawDataProvider::reloadData(){
	bool gotSome;
	if(dataMode == COLOR_DEPTH){	
		gotSome = worker->getRawData(rawDepth, rawColor, color2Depth, depth2Color, kin2_pass);//worker->getData(depthFloatBuffer, depthBuffer, colorBuffer);
	}
	else if (dataMode == COLOR_IR){
		gotSome = worker->getRawData_DEPTH_IR(rawDepth, rawIR, kin2_pass);
	}
	return gotSome;
}

void kinect2RawDataProvider::recomputeClouds(){

	if(dataMode == COLOR_DEPTH){
		auto & points = cloud->points;
		for(int i = 0; i < num_px_depth; i++){
			auto & p = points[i];
			p.z = rawDepth[i]/1000.f;
			kin_description.unproject(i%width,i/width, p.z, p.x, p.y);

			p.r = p.x * 100;
			p.g = p.y * 100;
			p.b = ((int) (p.z * 4000))%255;
		}
	}
	else if (dataMode == COLOR_IR)
	{		
		auto & points = cloud->points;
		for(int i = 0; i < num_px_depth; i++){
			auto & p = points[i];
			p.z = rawDepth[i]/1000.f;
			kin_description.unproject(i%width,i/width, p.z, p.x, p.y);

			//sensor noise is approx proportional to p.z^2, plus to the distance of the center...
			//so boosting up the signal by p.z^2 will give horrid noise in the bg.
			//also depth is not knoen everywhere.
			//intensity = std::max(std::min(rawIR[i]/65536.0 /(0.08*3) *p.z*p.z, 1.0), 0.0)*255;
			//intensity = std::max(std::min(rawIR[i]/65536.0 /(0.08*3) *p.z, 1.0), 0.0)*255;
			//intensity = std::max(std::min(rawIR[i]/65536.0 /(0.08*3), 1.0), 0.0)*255;
			//intensity = (std::log(rawIR[i])/std::log(2)) * 16;
			//intensity = std::log(rawIR[i])/std::log(2) * 32 ;

			byte intensity = std::max<float>(std::min<float>( std::log(static_cast<float>(rawIR[i]))/std::log(2) * 32 -256, 255.0),0.0);
			//seems to be good post processing...
			//byte intensity = std::max(std::min( std::log(static_cast<float>(rawIR[i]))/std::log(2) * 16 -128, 255.0),0.0);
			p.r = 	p.g = 	p.b = intensity;
		}
	}

}

scanning::Cloud::Ptr & kinect2RawDataProvider::getPointCloud(){
	return cloud;
}

