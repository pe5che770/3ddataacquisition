#version 150
// Default vertex shader

// Uniform variables, set in main program
uniform mat4 projection; 
uniform mat4 modelview;

// Input vertex attributes; passed from main program to shader 
// This corresponds
in vec4 position;
//this would declare and additional variable that could be passed to the shader.
in vec4 color;

// Output variables are passed to the fragment shader, or, if existent to the geometry shader
// and have to be declared as in variables within the next shader.
out vec4 frag_color;

void main()
{
	//compute a color and pass it to the fragment shader.
	frag_color = color;
	// Note: gl_Position is a default output variable containing
	// the transformed vertex position, this variable has to be computed
	// either in the vertex shader or in the geometry shader, if present.
	gl_Position = projection * modelview * position;
}
