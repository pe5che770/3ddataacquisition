#include "fileReader.h"
#include <iostream>
#include <sstream>
#include <algorithm>

#include <qimage.h>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>


bool str_compare(const std::string & st1, const std::string & st2){
	std::string temp, temp2;
	int number1, number2;
	std::stringstream ss(st1);
	
	//extract number 1;
	bool found = false;
	while(std::getline(ss,temp,'_')&&!found){
		std::stringstream iss(temp);
		while(std::getline(iss,temp2,'.')&&!found){
			if(std::stringstream(temp2) >> number1){
				found = true;
			}
		}
	}

	//extract number 2
	found = false;
	ss = std::stringstream(st2);
	while(std::getline(ss,temp,'_')&&!found){
		std::stringstream iss(temp);
		while(std::getline(iss,temp2,'.')&&!found){
			if(std::stringstream(temp2) >> number2){
				found = true;
			}
		}
	}

	return number1 < number2;
}

fileReader::fileReader(std::string folder,std::string basedepth , std::string baseimg)
{
	_folder = folder;
	_width = 0;
	_height = 0;

	//str_compare("blabla_bla_100.png","blabla_bla_9.png");
	std::stringstream ss; 
	ss << basedepth << ".*\\.png";
	listFiles(folder, ss.str(), files_depth);

	std::stringstream ss2; 
	ss2  << baseimg << ".*\\.png";
	listFiles(folder, ss2.str(), files_color);

	std::sort(files_depth.begin(), files_depth.end(), str_compare);
	std::sort(files_color.begin(), files_color.end(), str_compare);

	//peek for frame height and width
	if(files_depth.size() > 0){
		QImage depth_img;
		depth_img.load((_folder + "/" + files_depth[0]).c_str(), "PNG");
		_width = depth_img.width();
		_height = depth_img.height();
	}


}


fileReader::~fileReader(void)
{
}

void fileReader::listFiles(std::string folder, std::string query, std::vector<std::string> &target){

	const boost::regex my_filter( query );
	boost::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end
	for( boost::filesystem::directory_iterator i( folder ); i != end_itr; ++i )
	{
		// Skip if not a file
		if( !boost::filesystem::is_regular_file( i->status() ) ) continue;

		boost::smatch what;

		auto str = i->path().filename().string();
		// Skip if no match
		if( !boost::regex_match( str, what, my_filter ) ) continue;

		// File matches, store it
		target.push_back( str );
	}


	/*WIN32_FIND_DATA found_file;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	hFind = FindFirstFile(query.c_str(), & found_file);

	target.clear();
	while(hFind != INVALID_HANDLE_VALUE){
		target.push_back(std::string(found_file.cFileName));

		if(FindNextFile(hFind, &found_file) == FALSE){
			break;
		}
	}
	FindClose(hFind);*/

	std::cout << "Found " << target.size() << " files\n";


}

int fileReader::numberOfFrames(){
	return files_depth.size();
}

int fileReader::frameWidth(){
	return _width;
}
int fileReader::frameHeight(){
	return _height;
}

void fileReader::readFrame(int i, std::vector<scanning::UINT16> & depth, std::vector<scanning::RGBQUAD> & color){
	if(files_depth.size() > i){
		QImage depth_img;
		depth_img.load((_folder + "/" + files_depth[i]).c_str(), "PNG");

		depth.resize(depth_img.width() * depth_img.height());

		int idx=0;
		for(int j = 0; j < depth_img.height(); j++){
			//QRgb *rowData = (QRgb*)img.scanLine(j);
			QRgb *rowData_depth = (QRgb*) depth_img.scanLine(j);

			for(int i = 0; i < depth_img.width(); i++){
				depth[idx] = rowData_depth[i];
				idx++;
			}

		}
	}

	if(files_color.size() > i){
		QImage img;
		img.load((_folder + "/" + files_color[i]).c_str(), "PNG");

		color.resize(img.width() *img.height());

		int idx=0;
		scanning::RGBQUAD col;
		for(int j = 0; j < img.height(); j++){
			//QRgb *rowData = (QRgb*)img.scanLine(j);
			QRgb *rowData_depth = (QRgb*) img.scanLine(j);

			for(int i = 0; i < img.width(); i++){
				col.rgbRed =qRed(rowData_depth[i]);
				col.rgbGreen = qGreen(rowData_depth[i]);
				col.rgbBlue = qBlue(rowData_depth[i]);
				color[idx] = col;
				idx++;
			}

		}
	}

}

void fileReader::readFrame(int i, std::vector<scanning::UINT16> & depth, std::vector<scanning::UINT16> & color){
	if(files_depth.size() > i){
		QImage depth_img;
		depth_img.load((_folder + "/" + files_depth[i]).c_str(), "PNG");

		depth.resize(depth_img.width() * depth_img.height());

		int idx=0;
		for(int j = 0; j < depth_img.height(); j++){
			//QRgb *rowData = (QRgb*)img.scanLine(j);
			QRgb *rowData_depth = (QRgb*) depth_img.scanLine(j);

			for(int i = 0; i < depth_img.width(); i++){
				depth[idx] = rowData_depth[i];
				idx++;
			}

		}
	}

	if(files_color.size() > i){
		QImage img;
		img.load((_folder + "/" + files_color[i]).c_str(), "PNG");

		color.resize(img.width() *img.height());

		int idx=0;
		for(int j = 0; j < img.height(); j++){
			//QRgb *rowData = (QRgb*)img.scanLine(j);
			QRgb *rowData_col = (QRgb*) img.scanLine(j);

			for(int i = 0; i < img.width(); i++){
				color[idx] = rowData_col[i];
				idx++;
			}

		}
	}

}