#include "playBackProvider.h"
#include "fileReader.h"

//#include "KinectV2defs.h"
//#include "kinectCastMacro.h"
#include <limits>
#include<iostream>

#include <Windows.h>
#include <sstream>

std::string findSensorFile(std::string folder){
	std::stringstream ss; 
	ss << folder << "/*.sensor";
	std::string sensorFile = "";

	WIN32_FIND_DATA found_file;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	hFind = FindFirstFile(ss.str().c_str(), & found_file);

	if(hFind != INVALID_HANDLE_VALUE){
		sensorFile = (std::string(found_file.cFileName));

	}
	else{
		std::cout << "\nError: .sensor file not found!\n";
	}

	FindClose(hFind);

	std::stringstream ss2;
	ss2 << folder << "/" << sensorFile;
	return ss2.str();
}


playBackProvider::playBackProvider(std::string folder):
cloud(new scanning::Cloud())
{
	std::string sensor_file = findSensorFile(folder);
	sensorDescription = new RangeSensorDescription(sensor_file/**desc*/);
	reader = new fileReader(folder);
	frame = 0;
	
	cloud->resize(reader->frameWidth() * reader->frameHeight());
	cloud->width = reader->frameWidth();
	cloud->height = reader->frameHeight();
}


playBackProvider::~playBackProvider(void)
{
	delete sensorDescription;
	delete reader;
}

void playBackProvider::getRawBuffers(std::vector<UINT16> & depth, 
		std::vector<RGBQUAD> & alignedColor){
	depth = depthBuffer;
	alignedColor = colorBuffer;
}

bool playBackProvider::reloadData(){

	if(frame < reader->numberOfFrames()){
		reader->readFrame(frame,depthBuffer_,colorBuffer_);
		depthBuffer = depthBuffer_;
		colorBuffer.resize(colorBuffer_.size());
		
		//hack, for testing
		for(int i = 0; i < colorBuffer_.size();i++){
			colorBuffer[i].rgbRed = (BYTE) colorBuffer_[i].rgbRed;
			colorBuffer[i].rgbGreen = (BYTE) colorBuffer_[i].rgbGreen;
			colorBuffer[i].rgbBlue = (BYTE) colorBuffer_[i].rgbBlue;
		}

		frame++;
		return true;
	}
	return false;
}

void playBackProvider::recomputeClouds(){

	int width = reader->frameWidth();
	int numPx = reader->frameWidth()* reader->frameHeight();

	//ensure there is enough space allocated
	depthFloatBuffer.resize(numPx);
	normalFloatBuffer.resize(numPx*3);

	for(int i = 0; i < depthBuffer.size(); i++){
		depthFloatBuffer[i] = depthBuffer[i]/1000.f;
	}


	auto & points = cloud->points;
	float qnan = std::numeric_limits<float>::quiet_NaN();
	float dist;
	for(int i = 0; i < numPx; i++){
		auto & p = points[i];

		p.z = depthBuffer[i] / 1000.f;
		//p.x = CAST_I2X(i%width, p.z);
		//p.y = CAST_J2Y(i/width, p.z);
		sensorDescription->unproject(i%width,i/width,p.z,p.x,p.y);

		p.b = colorBuffer[i].rgbRed;
		p.g = colorBuffer[i].rgbGreen;
		p.r = colorBuffer[i].rgbBlue;


		//fix for flying pixels: no normal computation possible or too orthogonal
/*		dist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
		if(-(p.normal_z*p.z + p.normal_y*p.y + p.normal_x*p.x)/dist < 0.3 || p.normal_z*0!=0 || p.z==0){
			p.x= p.y = p.z = qnan;
		}*/
		
	}

}