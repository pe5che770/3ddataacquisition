#include "RangeSensorRecorder.h"
#include <iostream>
#include <sstream>
#include <qthreadpool.h>
#include "Sensors.h"
#include "kinect2RawDataProvider.h"
#include <fstream>

RangeSensorRecorder::RangeSensorRecorder(std::string outFolder, RangeSensorDescription * description, fileWriteTask_raw::mode mode):
	m(mode)
{
	outputFolder = outFolder;
	_description = description;
	isFirstFrame = true;
}


RangeSensorRecorder::~RangeSensorRecorder(void)
{
}

void RangeSensorRecorder::recordCurrentFrame(kinectProvider * provider, int frameNr){

	fileWriteTask * writeTask = new fileWriteTask(outputFolder,
		_description, frameNr);
	//copy the data
	provider->getRawBuffers(writeTask->depth, writeTask->color);
	//and dispatch the task. Will be deleted automagically.
	QThreadPool::globalInstance()->start(writeTask);
}

void RangeSensorRecorder::recordCurrentFrame(kinect2RawDataProvider * provider, int frameNr){

	fileWriteTask_raw * writeTask = new fileWriteTask_raw(outputFolder, frameNr, isFirstFrame,m);
	//copy the data
	provider->getRawBuffers(* writeTask->data);
	//and dispatch the task. Will be deleted automagically.
	QThreadPool::globalInstance()->start(writeTask, QThread::LowPriority);

	if(isFirstFrame)
	{
		isFirstFrame = false;
	}
}

void RangeSensorRecorder::saveDescription(){
	std::stringstream ss;
	ss << outputFolder << "/description.sensor";
	_description->saveToFile(ss.str());
}