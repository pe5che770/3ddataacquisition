#pragma once

#include "Definitions.h"
#include <Windows.h>
#include <Ole2.h>
#include <GL/GLU.h>

#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>
#include <fusion.h>

#include <math.h>
#include <vector>
#include "kinectProvider.h"


class kinect1Provider:public kinectProvider
{
private:

	kinectV1Description kin1_description;
	// Kinect variables
	int width, height;
	HANDLE depthStream, colorStream;
	INuiSensor* sensor;
	USHORT * data;

	std::vector<BYTE> colorData;//BYTE * colorData;
	std::vector<long> colorCoordinates;
	std::vector<float> colorCoordinates_float;
	std::vector<float> rawFloatFrame;
	scanning::Cloud::Ptr myCloud, filteredCloud;

public:
	kinect1Provider(providerType whatToLoad);
	virtual ~kinect1Provider(void);


	RangeSensorDescription * getDescription();
	virtual bool reloadData();
	virtual void recomputeClouds();

	virtual scanning::Cloud::Ptr & getPointCloud();
	void getRawBuffers(std::vector<UINT16> &depths, std::vector<RGBQUAD> & colors);

private:
		
	//resolve errors
	static void resolve(HRESULT & res);
	bool reloadDepthData();
	bool reloadDepthData(INuiSensor* sensor, HANDLE & depthStream, int maxTries, int timeOut);
	bool reloadColorData();
	bool reloadColorData(INuiSensor* sensor, HANDLE & depthStream, int maxTries, int timeOut);
	void copyDepthDataRGBA(GLubyte * data);
	void copyDepthData_meter(float * data);
	BYTE * getColorData();

	std::vector<float>& getXYToColorCoord();

	HRESULT reloadColorCoords();
	scanning::Cloud::Ptr & getNewPointCloud();
	static void getCloudFromDepth(scanning::Cloud & target, float * depth, RangeSensorDescription * desc);
	void computeNormals(scanning::Cloud::Ptr & myCloud);
	void bilateralFilter_cpu(scanning::Cloud::Ptr & cloud, scanning::Cloud::Ptr & target);

	
	//open a depth stream.
	bool initKinect();

};

