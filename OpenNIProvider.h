// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "kinectProvider.h"
#include "tuples.h"

#if (!defined USING_CMAKE) && (defined _MSC_VER)
#pragma comment(lib, "OpenNI2")
#endif

class OpenNIProvider : public kinectProvider
{
private:
	class PrivateData;
	PrivateData *data;
	tuple2i imageSize_rgb, imageSize_d;
	bool colorAvailable, depthAvailable;
	asusXtionProDescription asus_description;

	scanning::Cloud::Ptr myCloud;
	std::vector<UINT16> buffer_d; std::vector<RGBQUAD> buffer_rgb;
public:
	OpenNIProvider(const char *deviceURI = NULL, const bool useInternalCalibration = false,
		tuple2i imageSize_rgb = tuple2i(640, 480), tuple2i imageSize_d = tuple2i(640, 480));
	~OpenNIProvider();

	RangeSensorDescription * getDescription(){	return &asus_description;}
	virtual bool reloadData();
	virtual void recomputeClouds();

	virtual scanning::Cloud::Ptr & getPointCloud(){ return myCloud; }
	void getRawBuffers(std::vector<UINT16> &depths, std::vector<RGBQUAD> & colors);
};

