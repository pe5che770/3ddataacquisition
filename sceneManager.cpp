#include "sceneManager.h"
#include "glPointCloud.h"
#include <iostream>
/*#include "glExampleTriangles.h"
#include "glInstance.h"
#include "glSurflets.h"
#include "glCorrespondences.h"
#include "glSurfelVariance.h"
#include "glTriMesh.h"
#include "glEnhancedSurfelModel.h"*/

sceneManager::sceneManager(void)
{
	eye.setX(0); eye.setY(0), eye.setZ(0);
	lookAt.setX(0);lookAt.setY(0);lookAt.setZ(1);

	cam.setToIdentity();
	cam.lookAt(eye, lookAt, QVector3D(0,-1,0));
	


//	displayables.push_back(new glExampleTriangles());

	//prov = new kinectProvider();
	//displayables.push_back(new glInstance(new glKinectDepth(prov)));
	//displayables.push_back(new glInstance(new glKinectCloud(prov)));
//	displayables.push_back(new glInstance(new glExampleTriangles()));
}


sceneManager::~sceneManager(void)
{
	for(int i =0; i < displayables.size(); i++){
		delete displayables[i];
	}
	//delete prov;
}

QMatrix4x4 & sceneManager::getCam()
{
	return this->cam;
}

int sceneManager::numberDisplayables()
{
	return this->displayables.size();
}

glDisplayable * sceneManager::get(int i)
{

	return displayables[i];
}

glDisplayable * sceneManager::get(std::string name)
{
	auto cl = displayables_by_name.find(name);
	if(cl == displayables_by_name.end()){
		return NULL;
	}
	return displayables_by_name[name];
}

void sceneManager::displayables_push_back(std::string param1, glInstance * instance){
	displayables.push_back(instance);

	auto cl = displayables_by_name.find(param1);
	if(cl != displayables_by_name.end()){
		std::cout << "\nWARNING: multiple displayables have the same name! (" <<param1 << ") \n";
	}
	displayables_by_name[param1] = instance;

	if(displayables.size() > 1){
		instance->setInstance2World(displayables[0]->getInstance2World());
	}

}
void sceneManager::onRotate(QMatrix4x4 & mat)
{
	for(int i = 0; i< displayables.size(); i++){
		displayables[i]->mult(mat);
	}
}


void sceneManager::onCamMovement( QMatrix4x4 & mat)
{
	cam= mat * cam;
}


void sceneManager::updateCloud( std::string param1, 
							   const scanning::Cloud::Ptr & cloud,
							   bool useColor )
{
	auto cl = clouds.find(param1);
	if(cl == clouds.end()){
		glPointCloud * newGlCloud = new glPointCloud(cloud, useColor);
		clouds[param1] = newGlCloud;

		displayables_push_back(param1, new glInstance(newGlCloud));
	}
	else{
		clouds[param1]->updateData(cloud, useColor);
	}
}



void sceneManager::addAndManage(glDisplayable * toDisplay)
{
	glInstance * inst = new glInstance(toDisplay);
	
	displayables.push_back(inst);
	if(displayables.size() > 1){
		inst->setInstance2World(displayables[0]->getInstance2World());
	}
}
