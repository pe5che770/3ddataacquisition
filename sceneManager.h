#pragma once
#include <QMatrix4x4>
#include <QVector3D>
#include "glDisplayable.h"
#include <vector>
#include "glInstance.h"
#include "Definitions.h"
#include <map>



class glPointCloud;

class sceneManager:public QObject
{
	Q_OBJECT
private:
	std::map<std::string, glPointCloud*> clouds;
	std::map<std::string, glDisplayable*> displayables_by_name;

	std::vector<bool> showDisplayable;

public:
	QVector3D eye, lookAt;
	QMatrix4x4 cam;//, proj;
	std::vector<glInstance*> displayables;
public:
	sceneManager(void);
	~sceneManager(void);

	QMatrix4x4 & getCam();
	int numberDisplayables();
	glDisplayable * get(int i);
	glDisplayable * get(std::string name);
	//QMatrix4x4 & getProj();

	void addAndManage(glDisplayable * toDisplay);

	//void updateCloud(std::string  param1, std::vector<Eigen::Vector3f> & ps, std::vector<Eigen::Vector3f> & cols );
	void updateCloud( std::string  param1, const scanning::Cloud::Ptr & cloud, bool useColor = true );
	
		

private:
	void displayables_push_back(std::string param1, glInstance * glThing);

public Q_SLOTS:
	void onRotate(QMatrix4x4 & );
	void onCamMovement(QMatrix4x4 &);
};

