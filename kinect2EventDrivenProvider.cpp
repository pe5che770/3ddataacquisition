#include "kinect2EventDrivenProvider.h"

/*#include "KinectV2defs.h"
#include "kinectCastMacro.h"*/


#include <qmutex.h>
#include <qthread.h>
#include "kinect2Worker.h"

#include "Sensors.h"

kinect2EventDrivenProvider::kinect2EventDrivenProvider(providerType whatToLoad):
	kinectProvider(whatToLoad),
	cloud(new scanning::Cloud()),
	filteredCloud(new scanning::Cloud())
{
	
	width = kin_description.width(); //WIDTH
	height = kin_description.height(); //HEIGHT
	num_px_depth = width * height;
	

	depthFloatBuffer.resize(num_px_depth, 0);
	colorBuffer.resize(num_px_depth);
	normalFloatBuffer.resize(num_px_depth*3, 0);


	cloud->resize(width * height);
	cloud->height = height;
	cloud->width = width;

	
	//Set up the worker which will get the data in its own thread.
	//connect signals such that deletion will work properly.
	thread = new QThread();
	worker = new kinect2Worker(&kin_description);
	worker->moveToThread(thread);
	
	QObject::connect(thread, SIGNAL(started()), worker, SLOT(mainLoop()));
	QObject::connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
	QObject::connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
}


kinect2EventDrivenProvider::~kinect2EventDrivenProvider(void)
{
	//worker: stop. This will trigger finished() and then the deleteLater()
	//such that both worker and thread are disposed gracefully.
	worker->shutDownGracefully();

}



bool kinect2EventDrivenProvider::reloadData(){
	bool gotSome = worker->getData(depthFloatBuffer, depthBuffer, colorBuffer);
	return gotSome;
}

void kinect2EventDrivenProvider::recomputeClouds(){

	

	kinectV2Description kin2;

	//m_pMapper->MapDepthFrameToColorSpace(width*height, &depthBuffer[0], width*height, &colorCoordinateBuffer[0]);
	//int color_x,color_y;
	auto & points = cloud->points;
	float qnan = std::numeric_limits<float>::quiet_NaN();
	float dist;
	for(int i = 0; i < num_px_depth; i++){
		auto & p = points[i];
		



		p.z = depthFloatBuffer[i];
		kin2.unproject(i%width,i/width, p.z, p.x, p.y);
		//p.x*=-1;
		/*
		p.z = depthFloatBuffer[i];
		p.x = CAST_I2X(i%width, p.z);
		p.y = CAST_J2Y(i/width, p.z);
		*/
		dist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);

				
		if( p.z==0){
			p.x= p.y = p.z = qnan;
		}

		p.r = colorBuffer[i].rgbBlue;
		p.g = colorBuffer[i].rgbGreen;
		p.b = colorBuffer[i].rgbRed;
		//make points with only depth data invalid.
		if(_whatToLoad == DEPTH_AND_COLOR){
			if(colorBuffer[i].rgbReserved == 1){
				p.x= p.y = p.z = qnan;
			}
		}
	}
}

scanning::Cloud::Ptr & kinect2EventDrivenProvider::getPointCloud(){
	return cloud;
}

