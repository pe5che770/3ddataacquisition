#pragma once

#include <QApplication>
#include <QObject>
#include "mainWindow.h"
#include "Definitions.h"

//#include "kinect2RawDataProvider.h"


/*class kinect2Provider;
class kinect2EventDrivenProvider;*/
class kinect2RawDataProvider;
class kinectProvider;
class RangeSensorRecorder;
class rawKinect2Data;

class demo_kinectSDK2: public QObject
{
	Q_OBJECT

public:
	enum demoType {KIN1, ASUS_XTION, KIN2_POLLING, KIN2_EVENTDRIVEN, FILEREADER, KIN2_RAWDATA, KIN2_RAWDATA_IR };
private:
	mainWindow * win;
	kinectProvider * kinect;
	kinect2RawDataProvider * kinectRaw;
	
	rawKinect2Data * rawData;
	
	std::string input_folder, output_folder;
	RangeSensorRecorder * recorder;

	QTimer * timer;

	int pass, start, totalTime;

	scanning::Cloud::Ptr kinectCloud;
	demoType demo;


	bool recordData;
	bool paused;
public:
	//demo_kinectSDK2(demoType);
	demo_kinectSDK2(demoType, std::string folder);
	~demo_kinectSDK2(void);

	void qInit();
	void qFree();
	void run();
public Q_SLOTS:
	void refresh(void);
	void refreshRawData(void);
	void startStopRecording();
	void startStopReading();
	void pause();
};

