#pragma once
#include <stdint.h>
#include "tuples.h"
#include <vector>
#include <memory>

namespace scanning{

	template<typename t>
	class basicCloud{
	public:
		typedef std::shared_ptr<basicCloud<t>> Ptr;
		int width, height;
		std::vector<t> points;
		basicCloud(){
			width = 0; height = 0;
		}
		~basicCloud(){}

		void resize(int sz){
			points.resize(sz);
			height = 1; width = sz;
		}
		void resize(int w, int h){
			points.resize(w*h);
			height = h; width = w;
		}

		t& at(int arg){
			return points[arg];
		}
		t& operator[](int arg){
			return points[arg];
		}

		int size(){
			return points.size();
		}
	};

	typedef  rgbpoint3f Point;
	typedef basicCloud<Point> Cloud;
	
	typedef uint16_t UINT16;
	typedef struct tagRGBQUAD {
		uint8_t    rgbBlue;
		uint8_t    rgbGreen;
		uint8_t    rgbRed;
		uint8_t    rgbReserved;
	} RGBQUAD;
}
