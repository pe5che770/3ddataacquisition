#pragma once

#define NOMINMAX
//#include "gl3w.h"
#include <QtGui>
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
//#include "DataCoordinator.h"
#include "glDisplay.h"

class mainWindow: public QMainWindow
{
	Q_OBJECT

public:
	mainWindow(QGLFormat & format);
	~mainWindow(void);
	void layoutGui();
	void setUpButtons();
	void addAction();
	sceneManager * getSceneManager();
	glDisplay* getGLWidget();
	QPushButton * getButton1();
	QPushButton * getButton2();
	QPushButton * getButton3();
	QPushButton * getButton4();

	void keyPressEvent(QKeyEvent* e);
	void printInfo(std::string info);

public Q_SLOTS:
	void printClicked();

Q_SIGNALS:
	void numericalKeyPressed(int value);
	void charKeyPressed(char value);

private:
	glDisplay * myGlDisp;
	QPushButton *b1, *b2,*b3,*b4;
	QLabel * l_out;
};

