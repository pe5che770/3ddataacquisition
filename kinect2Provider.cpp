#include "kinect2Provider.h"
#include <iostream>

/*#include "KinectV2defs.h"
#include "kinectCastMacro.h"*/


#include <limits>

kinect2Provider::kinect2Provider(providerType whatToLoad):
	kinectProvider(whatToLoad),
	cloud(new scanning::Cloud()),
	filteredCloud(new scanning::Cloud())
{
	m_pKinectSensor = NULL;
	m_pMapper = NULL;
	m_pColorReader = NULL;
	m_pDepthReader = NULL;
	width = height = width_color = height_color =  -1;
	v_fov = h_fov = 0;
	tan_h_fov_2 = tan_v_fov_2 = 0;
	num_px_color = num_px_depth = 0;
	initKinect();
}


kinect2Provider::~kinect2Provider(void)
{
	if(m_pKinectSensor != NULL){
		std::cout << "closing Kinext Stream\n";
		m_pKinectSensor->Close();
		m_pKinectSensor->Release();
	}
	if(m_pDepthReader != NULL){
		m_pDepthReader->Release();
	}
	if(m_pMapper != NULL){
		m_pMapper->Release();
	}
	if(m_pColorReader != NULL){
		m_pColorReader->Release();
	}
}

bool kinect2Provider::resolve(HRESULT & hr){
	if(!SUCCEEDED(hr)){
		std::cout <<"**Error in kinext2Provider: HRESULT " << hr << ": ";
		if(hr == E_PENDING){
			std::cout <<"Frame pending...";
		}
		std::cout << "\n";
		return false;
	}
	return true;
}



void kinect2Provider::initKinect(){
	HRESULT hr;

	std::cout << "initializing Kinect 2\n";
	hr = GetDefaultKinectSensor(&m_pKinectSensor);
	if(!resolve(hr)){
		return;
	}

	//open Sensor
	hr = m_pKinectSensor->Open();
	if(!resolve(hr)){
		return;
	}
	std::cout << "Kinect 2: initialized!\n Opening Depth Stream...\n";
	
	//Define Source and open Reader for that Source.
	//alternative would be a mutisource frame reader. But color fps might degenerate to 15 fps
	IDepthFrameSource* pDepthFrameSource = NULL;
    if (resolve(hr))
    {
        hr = m_pKinectSensor->get_DepthFrameSource(&pDepthFrameSource);
    }
	if (resolve(hr))
    {
        hr = pDepthFrameSource->OpenReader(&m_pDepthReader);
	}
    if(pDepthFrameSource != NULL){
		pDepthFrameSource->Release();
	}

	//colorReader.
	IColorFrameSource* pColorFrameSource = NULL;
	if (resolve(hr))
    {
        hr = m_pKinectSensor->get_ColorFrameSource(&pColorFrameSource);
    }
	if (resolve(hr))
    {
        hr = pColorFrameSource->OpenReader(&m_pColorReader);
    }
	if(pColorFrameSource != NULL){
		pColorFrameSource->Release();
	}

	//get coordinatemapper
	if (resolve(hr))
    {
        hr = m_pKinectSensor->get_CoordinateMapper(& m_pMapper);
    }

	//get width, height fieldofview etc
	initDepthStreamProperties();
	initColorStreamProperties();

	//initialize everything...
	//"intrinsics"
	float deg2rad = 3.14159265359/180;
	tan_h_fov_2 = tan(h_fov*deg2rad/2);
	tan_v_fov_2 = tan(v_fov*deg2rad/2);
	center_i = (width -1)/2.f;
	center_j = (height -1)/2.f;

	std::cout << "width & height:" << width << " " << height << "\n";
	std::cout << "hfov & vfov:" << h_fov << " " << v_fov << "\n";


	//buffer
	depthBuffer.resize(num_px_depth, 0);
	depthFloatBuffer.resize(num_px_depth, 0);
	normalFloatBuffer.resize(num_px_depth*3, 0);
	colorBuffer.resize(num_px_color, RGBQUAD());
	colorCoordinateBuffer.resize(num_px_depth, ColorSpacePoint());

	//clouds...
	cloud->points.resize(num_px_depth, scanning::Point());
	cloud->width = width;
	cloud->height = height;
	filteredCloud->points.resize(num_px_depth, scanning::Point());
	filteredCloud->width = width;
	filteredCloud->height = height;

}

void kinect2Provider::initColorStreamProperties(){
	IColorFrame * pColorFrame= NULL;
	HRESULT hr;
	//peek into frame...
	//busy wait... honestly???????
	do{
		hr = m_pColorReader->AcquireLatestFrame(&pColorFrame);
		std::cout << ".";
		Sleep(10);
	}while(hr == E_PENDING);
	if(resolve(hr)){
		std::cout << "Yay got a Cframe....\n";
	}
	
	IFrameDescription * pFrameDescription = NULL;
	unsigned int bytesPerPx = 0;
	if(resolve(hr)){
		hr = pColorFrame->get_FrameDescription(&pFrameDescription);
	}
	if(resolve(hr)){
		hr = pFrameDescription->get_Width(&width_color);
        hr = pFrameDescription->get_Height(&height_color);
		hr = pFrameDescription->get_LengthInPixels(& num_px_color);
		hr = pFrameDescription->get_BytesPerPixel(& bytesPerPx);
	}

	std::cout << "Color: width & height:" << width_color << " " << height_color << "\n";
	std::cout << "num_px:" << num_px_color << "\n";
	std::cout << "bytesPer_px:" << bytesPerPx << "\n";

	if(pFrameDescription != NULL){
		pFrameDescription->Release();
	}
	if(pColorFrame != NULL){
		pColorFrame->Release();
	}
}
void kinect2Provider::initDepthStreamProperties(){
	//figure out intrinsics
	IDepthFrame *pDepthFrame = NULL;
	HRESULT hr;
	//peek into frame...
	//busy wait... honestly???????
	do{
		hr = m_pDepthReader->AcquireLatestFrame(&pDepthFrame);
		std::cout << ".";
		Sleep(10);
	}while(hr == E_PENDING);
	if(resolve(hr)){
		std::cout << "Yay got a frame....\n";
	}

	IFrameDescription* pFrameDescription = NULL;
	USHORT nDepthMinReliableDistance,nDepthMaxReliableDistance;
	unsigned int bytesPerPx;
	if (SUCCEEDED(hr))
    {
        hr = pDepthFrame->get_FrameDescription(&pFrameDescription);
    }

	if (SUCCEEDED(hr))
    {
		hr = pFrameDescription->get_Width(&width);
        hr = pFrameDescription->get_Height(&height);
		hr = pFrameDescription->get_HorizontalFieldOfView (&h_fov);
		hr = pFrameDescription->get_VerticalFieldOfView(&v_fov);
		hr = pFrameDescription->get_LengthInPixels(& num_px_depth);
		hr = pFrameDescription->get_BytesPerPixel(& bytesPerPx);
    }
	
	if (SUCCEEDED(hr))
    {
        hr = pDepthFrame->get_DepthMinReliableDistance(&nDepthMinReliableDistance);
		hr = pDepthFrame->get_DepthMaxReliableDistance(&nDepthMaxReliableDistance);
    }
    

	if(pFrameDescription != NULL){
		pFrameDescription->Release();
	}
	if(pDepthFrame != NULL){
		pDepthFrame->Release();
	}
}

bool kinect2Provider::reloadData(){
	IDepthFrame *pDepthFrame = NULL;
	HRESULT hr;
	TIMESPAN time1 = 0, time2 = 0;
	bool gotDepth= false;

	//acquire frame
	hr = m_pDepthReader->AcquireLatestFrame(&pDepthFrame);
	while(hr == E_PENDING){
		std::cout << "�";
		if(pDepthFrame != NULL){
			pDepthFrame->Release();
		}
		Sleep(10);
		hr = m_pDepthReader->AcquireLatestFrame(&pDepthFrame);
	}
	if(SUCCEEDED(hr)){
		//copy data
		pDepthFrame->CopyFrameDataToArray(num_px_depth, &(depthBuffer[0]));
		pDepthFrame->get_RelativeTime(&time1);
		std::cout << ":";
		gotDepth = true;
	}
	
	if(pDepthFrame != NULL){
		pDepthFrame->Release();
	}

	//acquire color
	IColorFrame *pColorFrame =  NULL;
	hr = m_pColorReader->AcquireLatestFrame(&pColorFrame);
	if(SUCCEEDED(hr)){
		//copy data
		RGBQUAD * pColorBuffer = &colorBuffer[0];
		UINT szInBytes = colorBuffer.size() * sizeof(RGBQUAD);

		hr = pColorFrame->CopyConvertedFrameDataToArray(szInBytes, 
			reinterpret_cast<BYTE*>(pColorBuffer), 
			ColorImageFormat_Rgba);
		pColorFrame->get_RelativeTime(&time2);
		std::cout << "!";
		if(time1 != 0 && time2!=0){
			std::cout << time1-time2;
		}
	}
	
	if(pColorFrame!= NULL){
		pColorFrame->Release();
	}//*/

	return gotDepth;
}


void kinect2Provider::recomputeClouds(){


	//refactor this....? No reason to pump floats instead of 16 bit ints to 
	//the gpu
	for(int i = 0; i < depthBuffer.size(); i++){
		depthFloatBuffer[i] = depthBuffer[i]/1000.f;
	}


	m_pMapper->MapDepthFrameToColorSpace(width*height, &depthBuffer[0], width*height, &colorCoordinateBuffer[0]);
	auto & points = cloud->points;
	int color_x,color_y;
	float qnan = std::numeric_limits<float>::quiet_NaN();
	float dist;
	for(int i = 0; i < num_px_depth; i++){
		auto & p = points[i];
		
		color_x = (int)(floor(colorCoordinateBuffer[i].X+ 0.5));
		color_y = (int)(floor(colorCoordinateBuffer[i].Y+ 0.5));

		p.z = depthBuffer[i] / 1000.f;//in meter
		kin2_description.unproject(i % width, i/width, p.z,p.x,p.y);
		/*p.x = cast_i2x(i%width, p.z);
		p.y = cast_j2y(i/width, p.z);*/
		dist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);


	

		//fix for flying pixels: no normal computation possible or too orthogonal
		if( p.z==0){
			p.x= p.y = p.z = qnan;
		}


		if ((color_x >= 0) && (color_x < width_color) && (color_y >= 0) && (color_y < height_color)){
			RGBQUAD & color = colorBuffer[color_x + width_color *color_y];
			p.r = color.rgbBlue;
			p.g = color.rgbGreen;
			p.b = color.rgbRed;
		}
		else{
			p.r = 255;
			p.g = 0;
			p.b = 0;
		}

	}
}

RangeSensorDescription * kinect2Provider::getDescription(){
	return &kin2_description;
}

/*inline float kinect2Provider::cast_i2x(int i, float z){
	//return -2 * (i - center_i)*tan_h_fov_2*z/width;
	return CAST_I2X(i,z);
}

inline float kinect2Provider::cast_j2y(int j, float z){
	//return -2 * (height - 1 - j - center_j)*tan_v_fov_2 * z/height;
	return CAST_J2Y(j,z);
}*/

scanning::Cloud::Ptr & kinect2Provider::getPointCloud(){
	return cloud;
}

scanning::Cloud::Ptr & kinect2Provider::getSmoothedCloud(){
	return filteredCloud;
}