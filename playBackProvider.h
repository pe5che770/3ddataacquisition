#pragma once
#include "Definitions.h"
#include <Windows.h>
#include "kinectprovider.h"
#include <string>
#include "Sensors.h"

class fileReader;
class cudaTool;

class playBackProvider :
	public kinectProvider
{

private:
	std::vector<UINT16> depthBuffer;
	std::vector<RGBQUAD> colorBuffer;
	std::vector<scanning::UINT16> depthBuffer_;
	std::vector<scanning::RGBQUAD> colorBuffer_;


	std::vector<float> depthFloatBuffer, normalFloatBuffer;

	scanning::Cloud::Ptr cloud;

	RangeSensorDescription * sensorDescription;
	fileReader * reader;
	cudaTool * cuda;
	int frame;

public:
	playBackProvider(std::string folder);
	~playBackProvider(void);

	RangeSensorDescription * getDescription(){
		return sensorDescription;
	}

	bool reloadData();

	void recomputeClouds();

	scanning::Cloud::Ptr & getPointCloud(){
		return cloud;
	}

	scanning::Cloud::Ptr & getSmoothedCloud(){
		return cloud;
	}

	void getRawBuffers(std::vector<UINT16> & depth, 
		std::vector<RGBQUAD> & alignedColor);

	void restart(){
		frame = 0;
	}
};
