#include "glPointCloud.h"
#include "glDebugging.h"
#include <iostream>
#include<assert.h>



glPointCloud::glPointCloud(const scanning::Cloud::Ptr & cloud, bool useColor = true):
	glDisplayable("Resources/default_color.vert", "Resources/default_color.frag"),
	m_posBuffer(QGLBuffer::VertexBuffer), 
	m_colBuffer(QGLBuffer::VertexBuffer),
	m_indexBuffer(QGLBuffer::IndexBuffer)
{
	updateData(cloud, useColor);
	isUpToDate = true;
}

glPointCloud::~glPointCloud(void)
{
}

void glPointCloud::glPrepare()
{
	prepareShaderProgram(/*"Resources/default_color.vert", "Resources/default_color.frag"*/);
	glDebugging::didIDoWrong();

	m_vao.create();
	m_vao.bind();
	glDebugging::didIDoWrong();

	setUpBuffer("position",m_posBuffer, pos,QGLBuffer::DynamicDraw);
	setUpBuffer("color",m_colBuffer, col,QGLBuffer::DynamicDraw);
	int bid = m_posBuffer.bufferId();
	
	setUpIndexBuffer(m_indexBuffer,ind,QGLBuffer::DynamicDraw);

	if((bid= m_indexBuffer.bufferId())<=0){
		std::cout << "merde!";
	}
	isUpToDate = true;
}

bool glPointCloud::isGlPrepared()
{
	return m_shader.isLinked();
}

void glPointCloud::glUpdate()
{
	isUpToDate = true;
	m_vao.bind();
	m_posBuffer.bind();
	m_posBuffer.allocate(&(pos[0].x)/**/, 3 * pos.size()* sizeof( float ) );

	m_colBuffer.bind();
	m_colBuffer.allocate(&(col[0].x)/**/, 3 * pos.size()* sizeof( float ) );

	m_indexBuffer.bind();
	m_indexBuffer.allocate(/*idx*/&(ind[0]), ind.size()* sizeof( int ));

}

bool glPointCloud::isGlUpToDate()
{
	return isUpToDate;
}

void glPointCloud::glDraw()
{
	m_shader.bind();
	m_vao.bind();
	m_indexBuffer.bind();

	glDrawElements(GL_POINTS,ind.size(), GL_UNSIGNED_INT, 0);
	glDebugging::didIDoWrong();

	m_vao.release();
	m_shader.release();
	//m_shader.disableAttributeArray("position");

	glDebugging::didIDoWrong();
}



void glPointCloud::updateData(const scanning::Cloud::Ptr & cloud, bool useColor)
{
	if(pos.size() != cloud->size()|| pos.size() == 0){
		//vs underflow
		int sz = (cloud->size() == 0? 1: cloud->size());
		pos.resize(sz, tuple3f());
		col.resize(sz, tuple3f());
		ind.resize(sz, 0);
	}
	for(int i = 0; i < cloud->size(); i++){
		pos[i].set((float) cloud->at(i).x,
			(float) cloud->at(i).y,
			(float) cloud->at(i).z);
		

			col[i].set((float) cloud->at(i).r / 255.f,
				(float) cloud->at(i).g / 255.f,
				(float) cloud->at(i).b / 255.f);

		ind[i] = i;
	}

	isUpToDate = false;
}
