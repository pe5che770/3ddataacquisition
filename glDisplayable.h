#pragma once
//#include "gl3w.h"
#include <QString>
#include <QtOpenGL/QGLShaderProgram>
#include <QtOpenGL/QGLBuffer>
#include "myVAO.h"

//#include "tuplef.h"
//#include "tuplei.h"
#include <QMatrix4x4>

#include "tuples.h"

class glDisplayable
{
public:
	glDisplayable(std::string file_vshader,
		std::string file_fshader,
		std::string file_gshader = "");
	virtual ~glDisplayable(void);

	virtual void glPrepare() = NULL;
	virtual bool isGlPrepared() = NULL;
	virtual void glUpdate()= NULL;
	virtual bool isGlUpToDate()= NULL;

	virtual void glDraw() = NULL;

	virtual QMatrix4x4 getModel2World(){
		QMatrix4x4 id;
		id.setToIdentity();
		return id;
	}

	virtual QGLShaderProgram & get_m_shader(){
		return m_shader;
	}

	//virtual void checkedThatShadersAreTreated() = NULL;

	//if any argument is "" it will be ignored
	void setShader(std::string vertShader, std::string fragShader, std::string geomShader = ""){
		m_vShader = (vertShader.size() == 0 ? m_vShader: vertShader.c_str());
		m_fShader = (fragShader.size() == 0 ? m_fShader : fragShader.c_str());
		m_gShader = geomShader.c_str();
	}

protected:

	//m_shader and prepareShaderProgram could be encapsulated;
	//programs should be shareable between displayables.
	QGLShaderProgram m_shader;
	//file names of the shaders to use
	QString m_vShader, m_fShader, m_gShader;

	//the displayables buffers are associated to the vao.
	myVAO m_vao;

	//prepare the shader program m_shader
	bool prepareShaderProgram(/* const QString & vspath, 
		const QString & fspath , 
		const QString & gspath = ""*/);

	//set up a QGLBuffer for the vao. if the vao was not created before,
	//it will be created now.
	void setUpBuffer( const char *name, 
		QGLBuffer & buffer, 
		std::vector<tuple3f> & values, 
		QGLBuffer::UsagePattern type);
	void setUpBuffer( const char *name, 
		QGLBuffer & buffer, 
		std::vector<float> & values, 
		QGLBuffer::UsagePattern type, int tupleSize = 1);
	void setUpIntBuffer(
		const char *name, 
		QGLBuffer & buffer, 
		std::vector<int> & values, 
		QGLBuffer::UsagePattern type);

	//the vao the buffer should be bound with.
	void setUpIndexBuffer(QGLBuffer & buffer, 
		std::vector<tuple3i> & values, 
		QGLBuffer::UsagePattern type);
	void setUpIndexBuffer(QGLBuffer & buffer, 
		std::vector<int> & values, 
		QGLBuffer::UsagePattern type);

	void enableBuffer(const char *name, QGLBuffer & buffer);
	void disableBuffer(const char * name);

public:
	void setUniformArray(const char* name, std::vector<QMatrix4x4> & mats, int sz);
	void setUniformValue(const char *name, QMatrix4x4 & mat);
	void setUniformValue(const char *name, QVector3D & vec);
	void setUniformValue(const char *name, float val);
	
};

