#pragma once
#include <qmatrix4x4.h>
#include "sensorDescriptionData.h"


class RangeSensorDescription
{
	//width and height in pixel
	int _width;
	int _height;
	//distortion parameters
	float k2,k4,k6;


	//diagonal distance between two samples on the plane orth(0,0,1) at (0,0,1).
	//needed to estimate the sample density at some depth z
	float _sample_distance;

	//Frustum
	QMatrix4x4 _frustum;
	QMatrix4x4 _viewPort;
	QMatrix4x4 _unproject;
//	QMatrix4x4 _project;


public:
	enum fileType {SENSOR, KIN2};

	RangeSensorDescription(int width, int height, QMatrix4x4 &frustum){
		setup(width, height, frustum);
		k2 = k4 = k6 = 0;
	}

	RangeSensorDescription(int width, int height, 
		float left, float right, float bottom, float top, 
		float near_, float far_){
			QMatrix4x4 frustum;
			frustum.setToIdentity();
			frustum.frustum(left,right,bottom,top,near_,far_);
			setup(width,height,frustum);
			k2 = k4 = k6 = 0;
	}

	RangeSensorDescription(int width, int height,
		float px, float py, float fx, float fy)
	{
		float _near = 0.1f; float  _far = 10.f;
		QMatrix4x4 frustum;
		frustum.setToIdentity();
		frustum.frustum((0 - px) / fx * _near, (width - 1 - px) / fx * _near, (0 - py) / fy * _near, (height - py) / fy * _near, _near, _far);
		setup(width, height, frustum);
		k2 = k4 = k6 = 0;
	}

	RangeSensorDescription(std::string fileName, fileType ft = SENSOR){
		if(ft == SENSOR){
			loadFromFile(fileName);
			k2 = k4 = k6 = 0;
		}
		else if(ft== KIN2)
		{
			loadFromFileKin2(fileName);
		}
	}


	virtual ~RangeSensorDescription(void){}

	//hack in order not to mix qt and cuda compiler
	sensorDescriptionData descriptionData(){
		sensorDescriptionData data;
		data._width = _width;
		data._height = _height;
		for(int i = 0; i < 16; i++){
			data._unproject[i] = _unproject.data()[i];
			data._frustum[i] =  _frustum.data()[i];
		}
		return data;
	}

	/*
	* Find the x,y coordinates corresponding to the depth value z at pixel position  
	* i (width), j (height).
	*/
	void unproject(int i, int j, float z, float& target_x, float & target_y)
	{
		QVector4D tmp = _unproject * QVector4D(i,j,z,1);
		target_x = -tmp.x()*z;///tmp.w();
		target_y = tmp.y()*z;///tmp.w();
	}


	void project(float x, float y, float z, int&i, int&j){
		QVector4D tmp = _frustum * QVector4D(x,-y,z,1);
		tmp = tmp / tmp.w();
		tmp = _viewPort * tmp;
		i = std::floor(tmp.x() + 0.5);
		j = std::floor(tmp.y() + 0.5);
	}

	//unrounded coordinates
	void project(float x, float y, float z, float&i, float&j){
		QVector4D tmp = _frustum * QVector4D(x,-y,z,1);
		tmp = tmp / tmp.w();
		tmp = _viewPort * tmp;
		i = tmp.x();
		j = tmp.y();
	}

	int height(){return _height;}
	int width(){return _width;}
	QMatrix4x4 frustum(){return _frustum;}
	float unitSampleDistance(){return _sample_distance;}

	void loadFromFile(std::string file);
	void loadFromFileKin2(std::string file);
	void saveToFile(std::string file);

private:
	void setup(int width, int height, QMatrix4x4 &frustum);
};




//////////////////////////////////////////////////////////////////////////////////
//Hard codded descriptors
//////////////////////////////////////////////////////////////////////////////////

class kinectV1Description: public RangeSensorDescription
{
public:
	//kinectV1Description():RangeSensorDescription(640,480,-0.056002690847f,0.056002690847f,-0.042002,0.042002,0.1,100){}
	kinectV1Description():RangeSensorDescription(640,480,0.056002690847f,-0.056002690847f,-0.042002f,0.042002f,0.1f,100){}
};

class kinectV2Description: public RangeSensorDescription
{
public:
	kinectV2Description():RangeSensorDescription(512,424,-0.06972f,0.069712f,-0.0577f,0.0577f,0.1f,100){}
};


class asusXtionProDescription: public RangeSensorDescription
{
public:
	asusXtionProDescription() :RangeSensorDescription(640, 480, 346.471f, 249.031f, 573.71f, 574.394f){}
	//RangeSensorDescription(640,480,-0.01625197138f,0.01625197138f,-0.0125655136f,0.0125655136f,0.1f,100){}
};

class EVADescription: public RangeSensorDescription
{
public:
	//EVADescription():RangeSensorDescription(350, 400, -0.0186,0.0196,-.022,.0268, 0.1,100){}
	//EVADescription():RangeSensorDescription(350, 500, -0.0186f,0.0196f,-.022f,.0268f, 0.1f,100){}
	//width needs to be a multiple of 64...
	EVADescription():RangeSensorDescription(192, 192, -0.0196f,0.0186f,-.0268f, .022f, 0.1f,100){}
};