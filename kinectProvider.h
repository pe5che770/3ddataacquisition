#pragma once
#include "Definitions.h"
#include "Sensors.h"
#include <Windows.h>

//kinect Interface
class kinectProvider{
public:
		enum providerType {DEPTH_ONLY, DEPTH_AND_COLOR};
protected:
	providerType _whatToLoad;
public:

	kinectProvider(providerType whatToLoad = DEPTH_ONLY){
		_whatToLoad = whatToLoad;
	}
	virtual ~kinectProvider(){}

	//get new data from the sensor and cast it to the cloud format
	virtual bool reloadData() = NULL;
	virtual void recomputeClouds()= NULL;

	//getter for a description of the frustum of the used sensor.
	virtual RangeSensorDescription* getDescription() = NULL;

	//getters and setters for depth data
	virtual scanning::Cloud::Ptr & getPointCloud() = NULL;
	virtual void getRawBuffers(std::vector<UINT16> & depth, 
		std::vector<RGBQUAD> & alignedColor) = NULL;

	//commodity method to create a point cloud from a depth array 
	static void computeCloudFromDepth(scanning::Cloud::Ptr  & data, float * depth, RangeSensorDescription * desc)
	{
		float z;
		int index = 0;
		float qnan = std::numeric_limits<float>::quiet_NaN();
		int height = desc->height();
		int width = desc->width();
		auto & points = data->points;
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){

				z = depth[index];
				desc->unproject(j, i, z, points[index].x, points[index].y);
				z = (z == 0 ? qnan : z);

				points[index].z = z;
				points[index].r = 1;
				points[index].g = 0;
				points[index].b = 0;

				index++;
			}
		}
	}
	static void computeCloudFromDepthInMM(scanning::Cloud::Ptr  & data, UINT16 * depth, RangeSensorDescription * desc, float c1, float c2)
	{
		float z;
		int index = 0;
		float qnan = std::numeric_limits<float>::quiet_NaN();
		int height = desc->height();
		int width = desc->width();
		auto & points = data->points;
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){

				z = depth[index] * c1 + c2;
				desc->unproject(j, i, z, points[index].x, points[index].y);
				z = (z == 0 ? qnan : z);

				points[index].z = z;
				points[index].r = 1;
				points[index].g = 0;
				points[index].b = 0;

				index++;
			}
		}
	}
};
